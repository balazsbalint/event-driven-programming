#ifndef MISCALWINDOW_H
#define MISCALWINDOW_H

#include <QMainWindow>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class MisCalWindow; }
QT_END_NAMESPACE


class MisCalWindow : public QMainWindow
{
    Q_OBJECT

public:
    MisCalWindow(QWidget *parent = nullptr);
    ~MisCalWindow();

private slots:
    void on_pushButton_clicked();
    void mozgat();
    void rightSlot();
    void leftSlot();

private:
    Ui::MisCalWindow *ui;
    int newinput;
    int capacity;
    int counter;
    int travels;
    bool side;
    void newgame();
    void moveright();
    void moveleft();
    void check();
    QVector<QPushButton*> shore01;
    QVector<QPushButton*> shore02;
    QVector<QPushButton*> boat;
};
#endif // MISCALWINDOW_H
