#include "MisCalWindow.h"
#include "ui_MisCalWindow.h"
#include <QMessageBox>
#include <QProcess>

MisCalWindow::MisCalWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MisCalWindow)
{
    ui->setupUi(this);
    this->setFixedSize(1043,731);
    //QObject::connect(ui->pushButton, SIGNAL(clicked()), this, SLOT((newGameSlot())));
}

MisCalWindow::~MisCalWindow()
{
    delete ui;
}

void MisCalWindow::mozgat()
{
    QObject* _sender = sender();
    QPushButton* gomb = qobject_cast<QPushButton*>(_sender);
    if(counter<capacity){
          if(!side){
             if(boat.contains(gomb))
             {
                shore01.push_back(gomb);
                boat.removeOne(gomb);
                ui->boat->removeWidget(gomb);
                ui->shore1->addWidget(gomb);
                --counter;
            }else if(shore01.contains(gomb))
            {
                boat.push_back(gomb);
                shore01.removeOne(gomb);
                ui->shore1->removeWidget(gomb);
                ui->boat->addWidget(gomb);
                ++counter;
            }
        }else{
              if(boat.contains(gomb))
             {
                shore02.push_back(gomb);
                boat.removeOne(gomb);
                ui->boat->removeWidget(gomb);
                ui->shore2->addWidget(gomb);
                --counter;
            }else if(shore02.contains(gomb))
            {
                boat.push_back(gomb);
                shore02.removeOne(gomb);
                ui->shore2->removeWidget(gomb);
                ui->boat->addWidget(gomb);
                ++counter;
            }
            }
      }else if(counter==capacity)
    {
        if(!side)
        {
            if(boat.contains(gomb))
            {
               shore01.push_back(gomb);
               boat.removeOne(gomb);
               ui->boat->removeWidget(gomb);
               ui->shore1->addWidget(gomb);
               --counter;
           }
        }else {
            if(boat.contains(gomb))
           {
              shore02.push_back(gomb);
              boat.removeOne(gomb);
              ui->boat->removeWidget(gomb);
              ui->shore2->addWidget(gomb);
              --counter;
          }
        }
    }

}

void MisCalWindow::moveright()
{
    if(counter>=1 && counter<=capacity && side==false){
        for(int i=0; i< counter; i++)
        {
            shore02.push_back(boat[i]);
            ui->boat->removeWidget(boat[i]);
            ui->shore2->addWidget(boat[i]);
        }
        boat.clear();
        counter = 0;
        ++travels;
        side= true;
        ui->label_8->setText(" JOBB");
        check();
     }

}

void MisCalWindow::moveleft()
{
    if(counter>=1 && counter<=capacity && side==true){
        for(int i=0; i< counter; i++)
        {
            shore01.push_back(boat[i]);
            ui->boat->removeWidget(boat[i]);
            ui->shore1->addWidget(boat[i]);
        }
        boat.clear();
        counter = 0;
        side = false;
        ui->label_8->setText("  BAL");
        ++travels;
        check();
    }
}

void MisCalWindow::on_pushButton_clicked()
{
   newinput = ui->spinBox->value();
   capacity = ui->boat_2->value();
   if(capacity> (newinput*2))
   {
       QMessageBox::StandardButton reply1 = QMessageBox::critical(this,tr("Ajjaj"), tr("Rosszul adtad meg a hajó kapacitását! Szeretnél új játékot?"), QMessageBox::Yes|QMessageBox::No);
       if(reply1 == QMessageBox::No)
       {
           QApplication::quit();
       }else
       {
           QProcess::startDetached(QApplication::applicationFilePath());
           exit(1);
       }
   }
   ui->lcd1->display(capacity);
   ui->lcd2->display(newinput*2);
   ui->spinBox->setValue(2);
   ui->boat_2->setValue(1);
   newgame();
}

void clearLayout(QLayout *layout)
{
    if (layout) {
        while(layout->count() > 0){
            QLayoutItem *item = layout->takeAt(0);
            QWidget* widget = item->widget();
            if(widget)
                delete widget;
            delete item;
        }
    }
}

void MisCalWindow::newgame(){
    side = false;
    ui->label_8->setText("  BAL");
    counter = 0;
    travels = 0;
    shore01.clear();
    shore02.clear();
    boat.clear();
    clearLayout(ui->shore1);
    clearLayout(ui->shore2);
    clearLayout(ui->boat);
    for(int i= 0; i< newinput; i++)
    {
        QPushButton* m = new QPushButton("M"+QString::number(i+1),this);
        QPushButton* c = new QPushButton("C"+QString::number(i+1),this);
        m->setFont(QFont("Rockwell", 12, QFont::Monospace));
        c->setFont(QFont("Rockwell",12, QFont::Monospace));
        m->setStyleSheet("background-color: rgb(255, 204, 153); color: rgb(153, 51, 51);");
        c->setStyleSheet("background-color: rgb(204, 0, 0); color: rgb(255, 255, 204);");
        shore01.push_back(m);
        shore01.push_back(c);
        QObject::connect(m, SIGNAL(clicked()), this, SLOT(mozgat()));
        QObject::connect(c, SIGNAL(clicked()), this, SLOT(mozgat()));
    }
    for(int i= 0; i< newinput*2; i++)
    {
        ui->shore1->addWidget(shore01[i]);
    }
    QObject::connect(ui->jobbra, SIGNAL(clicked()), this, SLOT(rightSlot()));
    QObject::connect(ui->balra, SIGNAL(clicked()), this, SLOT(leftSlot()));

}

void MisCalWindow::check()
{
    struct persons{
        int p1miss;
        int p1can;
        int p2miss;
        int p2can;
        int bmiss;
        int bcan;
    } p = {0,0,0,0,0,0};

    for(int i=0; i< shore01.size(); i++)
    {
        if(shore01[i]->text()[0] == "M"){
            ++p.p1miss;
        }if(shore01[i]->text()[0] == "C"){
            ++p.p1can;
        }
    }

    for(int i=0; i< shore02.size(); i++)
    {
        if(shore02[i]->text()[0] == "M"){
            ++p.p2miss;
        }if(shore02[i]->text()[0] == "C"){
            ++p.p2can;
        }
    }
    for(int i=0; i< counter; i++)
    {
        if(boat[i]->text()[0] == "M"){
            ++p.bmiss;
        }if(boat[i]->text()[0] == "C"){
            ++p.bcan;
        }
    }

    if((p.p1miss<p.p1can &&  p.p1miss>0) ||(p.bmiss<p.bcan &&  p.bmiss>0)||(p.p2miss<p.p2can  &&  p.p2miss>0))
    {
        QMessageBox::StandardButton reply = QMessageBox::critical(this,tr("H I B A"), tr("A kannibálok megették a misszionáriusokat! Szeretnél új játékot?"), QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
        {
            QApplication::quit();
        }else
        {
            QProcess::startDetached(QApplication::applicationFilePath());
            exit(1);
        }
    }
    QString message = "Átvittél mindenkit! Ehhez összesen: " + QString::number(travels) + " átszállásra volt szükség!  Szeretnél új játékot?";
    if(shore02.size() == newinput*2){
        QMessageBox::StandardButton valasz = QMessageBox::information(this,tr("Király vagy!"), message, QMessageBox::Yes|QMessageBox::No);
        if(valasz == QMessageBox::No)
        {
            QApplication::quit();
        }else
        {
            QProcess::startDetached(QApplication::applicationFilePath());
            exit(1);
        }
    }

}


void MisCalWindow::rightSlot()
{
    moveright();
}

void MisCalWindow::leftSlot()
{
    moveleft();
}
