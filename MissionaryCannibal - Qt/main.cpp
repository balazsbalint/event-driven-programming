#include "MisCalWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MisCalWindow w;
    w.show();
    return a.exec();
}
