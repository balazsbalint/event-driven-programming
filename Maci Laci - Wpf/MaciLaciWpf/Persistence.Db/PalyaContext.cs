﻿using System;
using System.Data.Entity;


namespace Beadandó.MaciLaci.Persistence.Db
{
    public class PalyaContext :DbContext
    {
        public PalyaContext(String connection)
            : base(connection)
        {
        }

        public DbSet<PalyaModel> Palyaelemek { get; set; }
    }
}
