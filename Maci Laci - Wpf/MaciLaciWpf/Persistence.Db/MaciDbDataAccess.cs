﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace Beadandó.MaciLaci.Persistence.Db
{
    public class MaciDbDataAccess : IMaciDataAccess
    {
        private MaciContext _context;
        public MaciDbDataAccess(String connection)
        {
            _context = new MaciContext(connection);
            _context.Database.CreateIfNotExists(); // adatbázis séma létrehozása, ha nem létezik
        }

        public async Task SaveAsync(String name, MaciTable table)
        {
            try
            {
                // játékmentés keresése azonos névvel
                Game overwriteGame = await _context.Games
                    .Include(g => g.Fields)
                    .SingleOrDefaultAsync(g => g.Name == name);
                if (overwriteGame != null)
                    _context.Games.Remove(overwriteGame); // törlés
                String nehezs = "";
                
                Game dbGame = new Game
                {
                    TableSize = table.meret,
                    Name = name,
                    Fordulszam = table.fordul.Length,
                    Jarorszam = table.jarorszam,
                    Óra = table._óra,
                    Perc = table._perc,
                    Másodperc = table._másodperc,
                    KosárKell = table.KosárKell,
                    KosárGyűjt = table.KosárGyűjt,
                    
                };

                dbGame.Fordul = new bool[dbGame.Fordulszam];
                dbGame.Fordul = table.fordul;

                for (Int32 i = 0; i < table.meret; ++i)
                {
                    for (Int32 j = 0; j < table.meret; ++j)
                    {
                        Field field = new Field
                        {
                            X = i,
                            Y = j,
                            Value = table.GetValue(i, j)
                        };
                        dbGame.Fields.Add(field);
                    }
                } // mezők mentése

                _context.Games.Add(dbGame); // mentés hozzáadása a perzisztálandó objektumokhoz
                await _context.SaveChangesAsync(); // mentés az adatbázisba
            }
            catch (Exception ex)
            {
                throw new MaciDataException();
            }
        }

        public async Task<MaciTable> LoadAsync(String name, Int32 meret)
        {
            try
            {
                Game game = await _context.Games
                    .Include(g => g.Fields)
                    .SingleAsync(g => g.Name == name); // játék állapot lekérdezése
                MaciTable table = new MaciTable(game.TableSize); // játéktábla modell létrehozása

                foreach (Field field in game.Fields) // mentett mezők feldolgozása
                {
                    table.SetValue(field.X, field.Y, field.Value);
                }
                if(game.TableSize != meret)
                {
                    throw new MaciDataException();
                }
                table.fordul = game.Fordul;
                table.jarorszam = game.Jarorszam;
                table._óra = game.Óra;
                table._perc = game.Perc;
                table._másodperc = game.Másodperc;
                table.KosárGyűjt = game.KosárGyűjt;
                table.KosárKell = game.KosárKell;
                if(table.meret== 11)
                {
                    table.fordul = new bool[] { game.Fordul[0], game.Fordul[1], game.Fordul[2] };
                    table.irányok = new int[3] {0,0,1 };
                }
                return table;
            }
            catch
            {
                throw new MaciDataException();
            }
        }

        public async Task<ICollection<SaveEntry>> ListAsync()
        {
            try
            {
                return await _context.Games
                    .OrderByDescending(g => g.Time) // rendezés mentési idő szerint csökkenő sorrendben
                    .Select(g => new SaveEntry { Name = g.Name, Time = g.Time }) // leképezés: Game => SaveEntry
                    .ToListAsync();
            }
            catch
            {
                throw new MaciDataException();
            }
        }
    }
}
