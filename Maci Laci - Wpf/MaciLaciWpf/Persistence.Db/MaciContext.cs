﻿using System;
using System.Data.Entity;

namespace Beadandó.MaciLaci.Persistence.Db
{
    class MaciContext : DbContext
	{
		public MaciContext(String connection)
            : base(connection)
    {
    }

    public DbSet<Game> Games { get; set; }
    public DbSet<Field> Fields { get; set; }
}
}
