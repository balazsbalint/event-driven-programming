﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Beadandó.MaciLaci.Persistence.Db
{
    class Game
    {
        /// <summary>
        /// Név, egyedi azonosító.
        /// </summary>
        [Key]
        [MaxLength(32)]
        public String Name { get; set; }

        
        /// <summary>
        /// Tábla mérete.
        /// </summary>
        public Int32 TableSize { get; set; }

        public Int32 Jarorszam { get; set; }
        public Int32 Fordulszam { get; set; }

        public bool[] Fordul { get; set; }
        public Int32 Óra { get; set; }

        public Int32 Perc { get; set; }

        public Int32 Másodperc { get; set; }

        public Int32 KosárKell { get; set; }

        public Int32 KosárGyűjt { get; set; }
        /// <summary>
        /// Mentés időpontja.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Játékmezők.
        /// </summary>
        public ICollection<Field> Fields { get; set; }

        public Game()
        {
            Fields = new List<Field>();
            Time = DateTime.Now;
        }
    }
}
