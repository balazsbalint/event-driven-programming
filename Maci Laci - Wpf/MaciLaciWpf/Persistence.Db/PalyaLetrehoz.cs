﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Beadandó.MaciLaci.Persistence;


namespace Beadandó.MaciLaci.Persistence.Db
{
    public class PalyaLetrehoz 
    {
        public PalyaContext _context;
        private bool volt;
        public PalyaLetrehoz(String connection)
        {
            volt = false;
            _context = new PalyaContext(connection);
            if (_context.Database.Exists())
            {
                volt = true;
            }
            _context.Database.CreateIfNotExists();
        }

        public Task<ICollection<SaveEntry>> ListAsync()
        {
            throw new NotImplementedException();
        }

        public Task<MaciTable> LoadAsync(string path)
        {
            throw new NotImplementedException();
        }

        public async Task SaveAsync(string path, MaciTable table)
        {
            if (!volt)
            {
                ///1.pálya
                PalyaModel asd = new PalyaModel { Value = 2, X = 0, Y = 8 , Game= 1};
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 1, Y = 2, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 2, Y = 7, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 3, Y = 1, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 4, Y = 3, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 5, Y = 5, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 6, Y = 3, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 7, Y = 7, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 8, Y = 8, Game = 1 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 3, X = 1, Y = 6, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 2, Y = 1, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 4, Y = 6, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 8, Y = 2, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 5, Y = 0, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 6, Y = 5, Game = 1 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 4, X = 2, Y = 3, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 7, Y = 8, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 5, Y = 1, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 8, Y = 0, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 7, Y = 4, Game = 1 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 0, Y = 6, Game = 1 };
                _context.Palyaelemek.Add(asd);
                ///2.pálya
               
                asd = new PalyaModel { Value = 2, X = 0, Y = 9, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 2, X = 8, Y = 0, Game = 2 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 1, X = 0, Y = 5, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 2, Y = 1, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 3, Y = 4, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 3, Y = 8, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 4, Y = 6, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 5, Y = 4, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 6, Y = 2, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 7, Y = 9, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 8, Y = 7, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 9, Y = 2, Game = 2 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 3, X = 1, Y = 6, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 5, Y = 0, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 4, Y = 3, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 6, Y = 9, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 9, Y = 9, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 0, Y = 2, Game = 2 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 4, X = 1, Y = 4, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 4, Y = 1, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 8, Y = 4, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 7, Y = 6, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 3, Y = 6, Game = 2 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 9, Y = 0, Game = 2 };
                _context.Palyaelemek.Add(asd);

                ///3.pálya
                asd = new PalyaModel { Value = 2, X = 0, Y = 10, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 2, X = 8, Y = 0, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 2, X = 3, Y = 4, Game = 3 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 1, X = 0, Y = 7, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 3, Y = 2, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 5, Y = 6, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 6, Y = 1, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 7, Y = 9, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 10, Y = 0, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 10, Y = 4, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 10, Y = 10, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 2, Y = 9, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 9, Y = 7, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 9, Y = 3, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 5, Y = 9, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 7, Y = 5, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 1, Y = 3, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 1, X = 3, Y = 5, Game = 3 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 3, X = 10, Y = 3, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 3, Y = 9, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 4, Y = 0, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 8, Y = 10, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 1, Y = 5, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 7, Y = 4, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 3, X = 10, Y = 7, Game = 3 };
                _context.Palyaelemek.Add(asd);

                asd = new PalyaModel { Value = 4, X = 4, Y = 4, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 10, Y = 9, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 8, Y = 6, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 5, Y = 10, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 2, Y = 1, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 7, Y = 2, Game = 3 };
                _context.Palyaelemek.Add(asd);
                asd = new PalyaModel { Value = 4, X = 6, Y = 7, Game = 3 };
                _context.Palyaelemek.Add(asd);
                await _context.SaveChangesAsync();
            }
        }
    }
}
