﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Beadandó.MaciLaci.Persistence.Db
{
    public class PalyaModel
    {
        [Key]
        public Int32 Id { get; set; }

        /// <summary>
        /// Vízszintes koordináta.
        /// </summary>
        public Int32 X { get; set; }
        /// <summary>
        /// Függőleges koordináta.
        /// </summary>
        public Int32 Y { get; set; }
        /// <summary>
        /// Tárolt érték.
        /// </summary>
        public Int32 Value { get; set; }

        public Int32 Game { get; set; }
    }
}
