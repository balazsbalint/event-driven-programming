﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Beadandó.MaciLaci.View;
using Beadandó.MaciLaci.ViewModel;
using Beadandó.MaciLaci.Persistence.Db;
using Beadandó.MaciLaci.Persistence;
using Beadandó.MaciLaci.Model;

namespace Beadandó.MaciLaci
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    ///
    public partial class App : Application
    {
        private MainWindow _view;
        private LoadWindow _loadWindow;
        private SaveWindow _saveWindow;
        private MaciModel _model;
        private MaciViewModel _viewModel;
        private DispatcherTimer _timer;
        public App()
        {
            Startup += new StartupEventHandler(App_Startup);
        }

        private async void App_Startup(object sender, StartupEventArgs e)
        {
            
            // perzisztencia létrehozása
            IMaciDataAccess dataAccess ;
            PalyaLetrehoz asd = new PalyaLetrehoz("MaciPalya");
            await asd.SaveAsync(null, null);
            dataAccess = new MaciDbDataAccess("MaciAdat"); // adatbázis alapú mentés

            
            // modell létrehozása
            _model = new MaciModel(9,dataAccess, asd);
            _model.GameOver += new EventHandler<MaciEventArgs>(Model_GameOver);
            
            
            // nézemodell létrehozása
            _viewModel = new MaciViewModel(_model);
            _viewModel.NewGame += new EventHandler(ViewModel_NewGame);
            _viewModel.LoadGameOpen += new EventHandler(ViewModel_LoadGameOpen);
            _viewModel.LoadGameClose += new EventHandler<String>(ViewModel_LoadGameClose);
            _viewModel.SaveGameOpen += new EventHandler(ViewModel_SaveGameOpen);
            _viewModel.SaveGameClose += new EventHandler<String>(ViewModel_SaveGameClose);
            
            // nézet létrehozása
            _view = new MainWindow();
            _view.DataContext = _viewModel;
            _view.Show();
            

            // időzítő létrehozása
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += new EventHandler(Timer_Tick);
            _timer.Start();
        }

        private void ViewModel_LoadGameOpen(object sender, System.EventArgs e)
        {
            Boolean restartTimer = _timer.IsEnabled;

            _timer.Stop();

            _viewModel.SelectedGame = null; // kezdetben nincsen kiválasztott elem

            _loadWindow = new LoadWindow(); // létrehozzuk a játék állapot betöltő ablakot
            _loadWindow.DataContext = _viewModel;
            _loadWindow.ShowDialog(); // megjelenítjük dialógusként

            if (restartTimer) // ha szükséges, elindítjuk az időzítőt
                _timer.Start();
        }

        /// <summary>
        /// Játék betöltésének eseménykezelője.
        /// </summary>
        private async void ViewModel_LoadGameClose(object sender, String name)
        {
            if (name != null)
            {
                try
                {
                    await _model.LoadGameAsync(name);
                }
                catch
                {
                    MessageBox.Show("Játék betöltése sikertelen! Lehet a pályaméret nem egyezik!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            _loadWindow.Close(); // játékállapot betöltőtő ablak bezárása
        }

        /// <summary>
        /// Játék mentés választó eseménykezelője.
        /// </summary>
        private void ViewModel_SaveGameOpen(object sender, EventArgs e)
        {
            Boolean restartTimer = _timer.IsEnabled;

            _timer.Stop();

            _viewModel.SelectedGame = null; // kezdetben nincsen kiválasztott elem
            _viewModel.NewName = String.Empty;

            _saveWindow = new SaveWindow(); // létrehozzuk a játék állapot mentő ablakot
            _saveWindow.DataContext = _viewModel;
            _saveWindow.ShowDialog(); // megjelenítjük dialógusként

            if (restartTimer) // ha szükséges, elindítjuk az időzítőt
                _timer.Start();
        }

        /// <summary>
        /// Játék mentésének eseménykezelője.
        /// </summary>
        private async void ViewModel_SaveGameClose(object sender, String name)
        {
            if (name != null)
            {
                try
                {
                    // felülírás ellenőrzése
                    var games = await _model.ListGamesAsync();
                    if (games.All(g => g.Name != name) ||
                        MessageBox.Show("Biztos, hogy felülírja a meglévő mentést?", "Sudoku",
                            MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        await _model.SaveGameAsync(name);
                    }
                }
                catch
                {
                    MessageBox.Show("Játék mentése sikertelen!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            _saveWindow.Close(); // játékállapot mentő ablak bezárása
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            _model.IdőEltelés();
            _viewModel.RefreshTable();
        }

        private void ViewModel_ExitGame(object sender, System.EventArgs e)
        {
            _view.Close(); // ablak bezárása
        }


        private void ViewModel_NewGame(object sender, EventArgs e)
        {
            if (_viewModel.Palyameret != 0)
            {
                _model.Restart(_viewModel.Palyameret);
                _timer.Start();
            }
        }

        private void Model_GameOver(object sender, MaciEventArgs e)
        {
            _timer.Stop();
            if (e.IsWon)
            {
                if (MessageBox.Show("Gratulálok, győztél!" + Environment.NewLine +
                                "Összesen " + e.Kosár + " kosárt gyűjtöttél és " +
                                e.Óra + ":" + e.Perc + ":" + e.Másodperc + " ideig játszottál." + Environment.NewLine + "Szeretnél új játékot?",
                                "Maci Laci játék",
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                    Application.Current.Shutdown();
                }
                else
                {
                    System.Windows.Application.Current.Shutdown();
                }
            }
            else
            {
                if (MessageBox.Show("Sajnálom, vesztettél, meglátott egy járőr!" + Environment.NewLine + "Szeretnél új játékot?",
                                "Maci Laci játék",
                                MessageBoxButton.YesNo,
                                MessageBoxImage.Error) == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                    Application.Current.Shutdown();
                }
                else
                {
                    System.Windows.Application.Current.Shutdown();
                }
            };
        }
    }
}
