﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beadandó.MaciLaci.Model;
using Beadandó.MaciLaci.Persistence;

namespace Beadandó.MaciLaci.ViewModel
{
    public class MaciViewModel : ViewModelBase
    {
        #region Fields

        public Int32 Palyameret;
        private MaciModel _model; // modell
        public SaveEntry _selectedGame;
        private String _newName = String.Empty;
        #endregion

        #region Properties

        public DelegateCommand MaciLeptet { get; private set; }
        /// <summary>
        /// Új játék kezdése parancs lekérdezése.
        /// </summary>
        public DelegateCommand NewGameCommand { get; private set; }

        /// <summary>
        /// Játék betöltés választó parancs lekérdezése.
        /// </summary>
        public DelegateCommand LoadGameOpenCommand { get; private set; }

        /// <summary>
        /// Játék betöltése parancs lekérdezése.
        /// </summary>
        public DelegateCommand LoadGameCloseCommand { get; private set; }

        /// <summary>
        /// Játék mentés választó parancs lekérdezése.
        /// </summary>
        public DelegateCommand SaveGameOpenCommand { get; private set; }

        /// <summary>
        /// Játék mentése parancs lekérdezése.
        /// </summary>
        public DelegateCommand SaveGameCloseCommand { get; private set; }

        /// <summary>
        /// Kilépés parancs lekérdezése.
        /// </summary>
        public DelegateCommand ExitCommand { get; private set; }

        /// <summary>
        /// Játékmező gyűjtemény lekérdezése.
        /// </summary>
        public ObservableCollection<MaciLaciField> Fields { get; set; }

        /// <summary>
        /// Lépések számának lekérdezése.
        /// </summary>
        public Int32 GameStepCount { get { return _model.GetKosár(); } }

        /// <summary>
        /// Fennmaradt játékidő lekérdezése.
        /// </summary>
        public String GameTime { get { return ""+_model.getÓra() + ":" +_model.getMinute()+":"+_model.getMp(); } }

        /// <summary>
        /// Alacsony nehézségi szint állapotának lekérdezése.
        /// </summary>
        

        public Boolean IsGameEasy
        {
            get { return Palyameret == 9; }
            set
            {
                if (Palyameret == 9)
                    return;

                Palyameret = 9;
                OnPropertyChanged("IsGameEasy");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameHard");
                
            }
        }

        /// <summary>
        /// Közepes nehézségi szint állapotának lekérdezése.
        /// </summary>
        public Boolean IsGameMedium
        {
            get { return Palyameret == 10; }
            set
            {
                if (Palyameret == 10)
                    return;

                Palyameret = 10;
                OnPropertyChanged("IsGameEasy");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameHard");
                
            }
        }

        /// <summary>
        /// Magas nehézségi szint állapotának lekérdezése.
        /// </summary>
        public Boolean IsGameHard
        {
            get { return Palyameret == 11; }
            set
            {
                if (Palyameret == 11)
                    return;

                Palyameret = 11;
                OnPropertyChanged("IsGameEasy");
                OnPropertyChanged("IsGameMedium");
                OnPropertyChanged("IsGameHard");
                
            }
        }
        
        /// <summary>
        /// Perzisztens játékállapot mentések lekérdezése.
        /// </summary>
      
        
    
        #endregion

        #region Events

        /// <summary>
        /// Új játék eseménye.
        /// </summary>
        public event EventHandler NewGame;

        /// <summary>
        /// Játék betöltés választásának eseménye.
        /// </summary>
        public event EventHandler LoadGameOpen;

        /// <summary>
        /// Játék betöltésének eseménye.
        /// </summary>
        public event EventHandler<String> LoadGameClose;

        /// <summary>
        /// Játék mentés választásának eseménye.
        /// </summary>
        public event EventHandler SaveGameOpen;

        /// <summary>
        /// Játék mentésének eseménye.
        /// </summary>
        public event EventHandler<String> SaveGameClose;

        public ObservableCollection<SaveEntry> Games { get; set; }
        public SaveEntry SelectedGame
        {
            get { return _selectedGame; }
            set
            {
                _selectedGame = value;
                if (_selectedGame != null)
                    NewName = String.Copy(_selectedGame.Name);

                OnPropertyChanged();
                LoadGameCloseCommand.RaiseCanExecuteChanged();
                SaveGameCloseCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Új játék mentés nevének lekérdezése.
        /// </summary>
        public String NewName
        {
            get { return _newName; }
            set
            {
                _newName = value;

                OnPropertyChanged();
                SaveGameCloseCommand.RaiseCanExecuteChanged();
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Sudoku nézetmodell példányosítása.
        /// </summary>
        /// <param name="model">A modell típusa.</param>
        public MaciViewModel(MaciModel model)
        {
            // játék csatlakoztatása
            _model = model;
            _model.PályaFeltölt();
            // parancsok kezelése
            NewGameCommand = new DelegateCommand(param => OnNewGame());
            MaciLeptet = new DelegateCommand(param => StepGame(Convert.ToInt32(param)));
            LoadGameOpenCommand = new DelegateCommand(async param =>
            {
                Games = new ObservableCollection<SaveEntry>(await _model.ListGamesAsync());
                OnLoadGameOpen();
            });
            LoadGameCloseCommand = new DelegateCommand(
                param => SelectedGame != null, // parancs végrehajthatóságának feltétele
                param => { OnLoadGameClose(SelectedGame.Name); });
            SaveGameOpenCommand = new DelegateCommand(async param =>
            {
                Games = new ObservableCollection<SaveEntry>(await _model.ListGamesAsync());
                OnSaveGameOpen();
            });
            SaveGameCloseCommand = new DelegateCommand(
                param => NewName.Length > 0, // parancs végrehajthatóságának feltétele
                param => { OnSaveGameClose(NewName); });
            
            // játéktábla létrehozása
            Fields = new ObservableCollection<MaciLaciField>();
            for (Int32 i = 0; i < _model.ModMéret; i++) // inicializáljuk a mezőket
            {
                for (Int32 j = 0; j < _model.ModMéret; j++)
                {
                    String img = "";
                    switch (_model.GetErtek(i, j))
                    {
                        case 0:
                            img = "";
                            break;
                        case 1:
                            img = "img\\kep2.png";
                            break;
                        case 2:
                            img = "img\\kep3.png";
                            break;
                        case 3:
                            img = "img\\kep5.png";
                            break;
                        case 4:
                            img = "img\\kep4.png";
                            break;
                        case 5:
                            img = "img\\kep.png";
                            break;
                    }
                    Fields.Add(new MaciLaciField
                    {
                        
                        Text = _model.GetErtek(i,j).ToString(),
                        X = i,
                        Y = j,
                        Img = img,
                });
                }
            }

            //RefreshTable();
        }

        private void StepGame(Int32 index)
        {
            _model.MaciLép((Irány)index);
            RefreshTable();
            OnPropertyChanged("GameStepCount"); // jelezzük a lépésszám változást
        }

        

        #endregion
        #region Private methods
        
        /// <summary>
        /// Tábla frissítése.
        /// </summary>
        public void RefreshTable()
        {

            for (int i = 0; i < _model.ModMéret * _model.ModMéret; i++)
            {
                Fields[i].Text = _model.GetErtek(Fields[i].X, Fields[i].Y).ToString();
                String img = "";
                switch (_model.GetErtek(Fields[i].X, Fields[i].Y))
                {
                    case 0:
                        img = "";
                        break;
                    case 1:
                        img = "img\\kep2.png";
                        break;
                    case 2:
                        img = "img\\kep3.png";
                        break;
                    case 3:
                        img = "img\\kep5.png";
                        break;
                    case 4:
                        img = "img\\kep4.png";
                        break;
                    case 5:
                        img = "img\\kep.png";
                        break;
                }
                Fields[i].Img = img;
            }
            OnPropertyChanged("GameTime");
            OnPropertyChanged("GameStepCount");
        }

        #endregion

        #region Event methods
   
        /// <summary>
        /// Új játék indításának eseménykiváltása.
        /// </summary>
        private void OnNewGame()
        {
            if (NewGame != null)
            {
                NewGame(this, EventArgs.Empty);
                Fields.Clear();
                for (Int32 i = 0; i < _model.ModMéret; i++) // inicializáljuk a mezőket
                {
                    for (Int32 j = 0; j < _model.ModMéret; j++)
                    {
                        string img = "";
                        switch (_model.GetErtek(i, j))
                        {
                            case 0:
                                img = "";
                                break;
                            case 1:
                                img = "img\\kep2.png";
                                break;
                            case 2:
                                img = "img\\kep3.png";
                                break;
                            case 3:
                                img = "img\\kep5.png";
                                break;
                            case 4:
                                img = "img\\kep4.png";
                                break;
                            case 5:
                                img = "img\\kep.png";
                                break;
                        }
                        Fields.Add(new MaciLaciField
                        {

                            Text = _model.GetErtek(i, j).ToString(),
                            X = i,
                            Y = j,
                            Img = img
                        });
                    }
                }
            }
           
        }
        
        /// <summary>
        /// Játék betöltés választásának eseménykiváltása.
        /// </summary>
        private void OnLoadGameOpen()
        {
            if (LoadGameOpen != null)
                LoadGameOpen(this, EventArgs.Empty);
        }

        /// <summary>
        /// Játék betöltésének eseménykiváltása.
        /// </summary>
        private void OnLoadGameClose(String name)
        {
            if (LoadGameClose != null)
                LoadGameClose(this, name);
        }

        /// <summary>
        /// Játék mentés választásának eseménykiváltása.
        /// </summary>
        private void OnSaveGameOpen()
        {
            if (SaveGameOpen != null)
                SaveGameOpen(this, EventArgs.Empty);
        }

        /// <summary>
        /// Játék mentésének eseménykiváltása.
        /// </summary>
        private void OnSaveGameClose(String name)
        {
            if (SaveGameClose != null)
                SaveGameClose(this, name);
        }

        
        #endregion
    
    }
}
