﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beadandó.MaciLaci.Model
{
    public class MaciEventArgs : EventArgs
    {
        private Int32 _hour;
        private Int32 _minute;
        private Int32 _second;
        private Int32 _kosarak;
        private Boolean _isWon;

        /// <summary>
        /// Játékidő lekérdezése.
        /// </summary>
        public Int32 Óra { get { return _hour; } }

        public Int32 Perc { get { return _minute; } }

        public Int32 Másodperc { get { return _second; } }

        /// <summary>
        /// Játéklépések számának lekérdezése.
        /// </summary>
        public Int32 Kosár { get { return _kosarak; } }

        /// <summary>
        /// Győzelem lekérdezése.
        /// </summary>
        public Boolean IsWon { get { return _isWon; } }

        /// <summary>
        /// Sudoku eseményargumentum példányosítása.
        /// </summary>
        /// <param name="isWon">Győzelem lekérdezése.</param>
        /// <param name="gameStepCount">Lépésszám.</param>
        /// <param name="gameTime">Játékidő.</param>
        public MaciEventArgs(Boolean isWon, Int32 gameStepCount, Int32 hour, Int32 minute, Int32 second)
        {
            _isWon = isWon;
            _kosarak = gameStepCount;
            _hour = hour;
            _second = second;
            _minute = minute;
        }
    }
}

