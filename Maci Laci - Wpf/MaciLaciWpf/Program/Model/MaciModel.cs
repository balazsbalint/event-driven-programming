﻿using System;
using System.Collections.Generic;
using Beadandó.MaciLaci.Persistence;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Beadandó.MaciLaci.Persistence.Db;

namespace Beadandó.MaciLaci.Model
{
    public enum FieldTypes { Üres, Kosár, Járőr, Fa, Kő, Maci }
    public enum Irány {Felfele, Lefele, Jobbra, Balra }

    public class MaciModel
    {
        public MaciTable _table;
        public IMaciDataAccess _data;

        public event EventHandler<MaciEventArgs> GameOver;

        PalyaLetrehoz asd;
        public Int32 getMp() { return _table._másodperc; }
        public Int32 getMinute() { return _table._perc; }
        public Int32 getÓra() { return _table._óra; }

        public void IdőEltelés()
        {
            ++_table._másodperc;
            if (_table._másodperc % 2 == 0)
            {
                JárőrLéptet();
            }
            if (_table._másodperc == 60)
            {
                _table._másodperc -= 60;
                ++_table._perc;
                if (_table._perc == 60)
                {
                    _table._perc -= 60;
                    ++_table._óra;
                }
            }

        }
        public void NewGame()
        {
            _table.newGame();
            _table._másodperc = 0;
            _table._perc = 0;
            _table._óra = 0;
        }

        public void setMéret(int value)
        {
            _table.meret = value;
        }

        public MaciModel(Int32 value, IMaciDataAccess dataAccess, PalyaLetrehoz palya)
        {
            _table = new MaciTable(value);
            //asd = palya;
            _data = dataAccess;
            
        }

        public void Restart(Int32 value)
        {
            _table = new MaciTable(value);
            Console.WriteLine("" + _table.meret);
            PályaFeltölt();
        }
        

        public int ModMéret { get { return _table.meret; } }

        public Int32 GetErtek(Int32 x, Int32 y)
        {
            if (x < 0 || x >= _table.meret)
                throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
            if (y < 0 || y >= _table.meret)
                throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");
            return _table.GetValue(x,y);
        }
        public void PályaFeltölt()
        {
            PalyaLetrehoz asd = new PalyaLetrehoz("MaciPalya");
             asd.SaveAsync(null, null);
            if (_table.meret == 9)
            {

                _table.KosárKell = 8;
                _table.KosárGyűjt = 0;
            }
            else if (_table.meret == 10)
            {

                _table.KosárKell = 10;
                _table.KosárGyűjt = 0;
            }
            else if (_table.meret == 11)
            {

                _table.KosárKell = 15;
                _table.KosárGyűjt = 0;
            }
            string sor;
            _table.SetValue(0, 0, 5);
            _table.SetMaci(0, 0);
            if (_table.meret == 9)
            {
                for (int i = 1; i < 22; i++)
                {
                    _table.SetValue(asd._context.Palyaelemek.Find(i).X, asd._context.Palyaelemek.Find(i).Y, asd._context.Palyaelemek.Find(i).Value);
                }
            }
            else if (_table.meret == 10)
            {
                for (int i = 22; i < 46; i++)
                {
                    _table.SetValue(asd._context.Palyaelemek.Find(i).X, asd._context.Palyaelemek.Find(i).Y, asd._context.Palyaelemek.Find(i).Value);
                }
            }
            else if (_table.meret == 11)
            {
                for (int i = 46; i < 77; i++)
                {
                    _table.SetValue(asd._context.Palyaelemek.Find(i).X, asd._context.Palyaelemek.Find(i).Y, asd._context.Palyaelemek.Find(i).Value);
                }
            }

        }
        public Int32 GetKosár()
        {
            return _table.KosárGyűjt;
        }

        public void MaciLép(Irány irany)
        {
            JárőrEllenőrzés();
            if (irany == Irány.Lefele)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if(x == _table.meret -1)
                {

                }
                else if (_table.GetValue(x+1,y) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x + 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x + 1, y);
                }
                else if (_table.GetValue(x + 1, y) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x + 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x + 1, y);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
                
            }
            else if(irany == Irány.Felfele)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if(x == 0)
                {

                }
                else if (_table.GetValue(x -1, y) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x - 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x - 1, y);
                }
                else if (_table.GetValue(x - 1, y) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x - 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x - 1, y);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
            }
            else if(irany == Irány.Jobbra)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if (y == _table.meret-1)
                {

                }
                else if (_table.GetValue(x, y+1) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x, y+1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y+1);
                }
                else if (_table.GetValue(x, y+1) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x , y + 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x , y + 1);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    
                }
            }
            else if (irany == Irány.Balra)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if (y == 0)
                {

                }
                else if (_table.GetValue(x, y - 1) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x, y - 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y - 1);
                }
                else if (_table.GetValue(x, y - 1) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x, y - 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y - 1);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    
                }
            }
            JárőrEllenőrzés();
            if (_table.KosárGyűjt == _table.KosárKell)
            {
                GameOver(this, new MaciEventArgs(true, _table.KosárGyűjt, getÓra(), getMinute(), getMp()));
            }
            
        }

        public bool Pause
        {
            get { return _table.pause; }
            set { _table.pause = value; }

        }
        public void JárőrEllenőrzés()
        {
            for (int i = 0; i < _table._jarorok.Length; i++)
            {
                int x = _table._jarorok[i].x;
                int y = _table._jarorok[i].y;
                for (int j = Math.Max(0,x-1); j <Math.Min(_table.meret,x+2) ; j++)
                {
                    for (int k = Math.Max(0, y - 1); k < Math.Min(_table.meret, y + 2); k++)
                    {
                        if(_table.GetValue(j,k) == (int)FieldTypes.Maci)
                        {
                            GameOver(this, new MaciEventArgs(false,_table.KosárGyűjt,getÓra(),getMinute(),getMp()));
                        }
                    }
                }
            }
            
        }


        public void JárőrLéptet()
        {
            JárőrEllenőrzés();
            for (int i = 0; i < _table._jarorok.Length; i++)
            {
                if (_table.irányok[i] == 0)
                {
                    if (_table.fordul[i])
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (a == _table.meret - 1)
                        {
                            _table.fordul[i] = false;
                        }
                        else if (_table.GetValue(a + 1, b) == 0)
                        {
                            _table.SetValueAtJárőrLép(a + 1, b, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].x = a + 1;
                        }
                        else if (_table.GetValue(a + 1, b) == 3 || _table.GetValue(a + 1, b) == 4)
                        {
                            _table.fordul[i] = false;
                        }
                    }
                    else
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (a == 0)
                        {
                            _table.fordul[i] = true;
                        }
                        else if (_table.GetValue(a - 1, b) == 0)
                        {
                            _table.SetValueAtJárőrLép(a - 1, b, 2);
                            _table.SetValueAtJárőrLép(a , b, 0);
                            _table._jarorok[i].x = a - 1;
                        }
                        else if (_table.GetValue(a - 1, b) == 3 || _table.GetValue(a - 1, b) == 4)
                        {
                            _table.fordul[i] = true;
                        }
                    }
                }
                else if (_table.irányok[i] == 1)
                {
                    if (_table.fordul[i])
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (b == _table.meret - 1)
                        {
                            _table.fordul[i] = false;
                        }
                        else if (_table.GetValue(a, b + 1) == 0)
                        {
                            _table.SetValueAtJárőrLép(a, b + 1, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].y = b + 1;
                        }
                        else if (_table.GetValue(a, b + 1) == 3 || _table.GetValue(a, b + 1) == 4)
                        {
                            _table.fordul[i] = false;
                        }
                    }
                    else
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (b == 0)
                        {
                            _table.fordul[i] = true;
                        }
                        else if (_table.GetValue(a, b - 1) == 0)
                        {
                            _table.SetValueAtJárőrLép(a, b-1, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].y = b - 1;
                        }
                        else if (_table.GetValue(a, b - 1) == 3 || _table.GetValue(a, b - 1) == 4)
                        {
                            _table.fordul[i] = true;
                        }
                    }
                }
            }
            JárőrEllenőrzés();

        }


        public async Task<ICollection<SaveEntry>> ListGamesAsync()
        {
            if (_data == null)
                throw new InvalidOperationException("No data access is provided.");

            return await _data.ListAsync();
        }

        public async Task SaveGameAsync(String name)
        {
            if (_data == null)
                throw new InvalidOperationException("No data access is provided.");

            await _data.SaveAsync(name, _table);
        }

        public async Task LoadGameAsync(String name)
        {
            if (_data == null)
                throw new InvalidOperationException("No data access is provided.");

            _table = await _data.LoadAsync(name, _table.meret);

           
        }


    }
}
