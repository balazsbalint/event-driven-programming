﻿using System;

namespace Beadandó.MaciLaci.Persistence
{
    /// <summary>
    /// Sudoku adatelérés kivétel típusa.
    /// </summary>
    public class MaciDataException : Exception
    {
        /// <summary>
        /// Sudoku adatelérés kivétel példányosítása.
        /// </summary>
        public MaciDataException(int param) { }
        public MaciDataException() { }
    }
}
