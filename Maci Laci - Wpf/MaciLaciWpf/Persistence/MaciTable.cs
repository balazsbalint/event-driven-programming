﻿using System;

namespace Beadandó.MaciLaci.Persistence
{
    public enum FieldTypes { Üres, Kosár, Járőr, Fa, Kő, Maci }
    public struct Maci
    {
        public int x;
        public int y;
    }
    public class MaciTable
    {
        Maci m = new Maci();
        private Int32 kosarkell;
        private Int32 gyujtottkosarak;
        public Int32 meret;
        public bool pause = false;
        public Int32 _másodperc;
        public Int32 _perc;
        public Int32 _óra;
        private Int32[,] _ertekek;
        public Maci[] _jarorok;
        public Int32 jarorszam;
        public int[] irányok;
        public bool[] fordul;
        public Int32 KosárKell { get
            {
                return kosarkell;
            }
            set
            {
                kosarkell = value;
            }
        }
        public Int32 KosárGyűjt
        {
            get
            {
                return gyujtottkosarak;
            }
            set
            {
                gyujtottkosarak = value;
            }
        }

        public void SetMaci(int a, int b)
        {
            m.x = a;
            m.y = b;
        }
        public int GetMaciX()
        {
            return m.x;
        }
        public int GetMaciY()
        {
            return m.y;
        }
        
        public MaciTable(Int32 oldalmeret)
        {
            if (oldalmeret < 0)
                throw new ArgumentOutOfRangeException("The table size is less than 0.", "tableSize");
            meret = oldalmeret;
            newGame();
        }

        public void newGame()
        {
            _ertekek = new Int32[meret, meret];
            if (meret == 9)
            {
                _jarorok = new Maci[1];
                irányok = new int[1] { 0 };
                fordul = new bool[1] { true };
            }
            else if (meret == 10)
            {
                _jarorok = new Maci[2];
                irányok = new int[2] { 0, 1 };
                fordul = new bool[2] { true, true };
            }
            else if (meret == 11)
            {
                _jarorok = new Maci[3];
                irányok = new int[3] { 0, 1, 0 };
                fordul = new bool[3] { true, true, false };
            }
        }
  
        public void SetValue(Int32 a, Int32 b, Int32 value)
        {
            if (a < 0 || a >= _ertekek.GetLength(0))
                throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
            if (b < 0 || b >= _ertekek.GetLength(1))
                throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");
            if (value < 0 || value > _ertekek.GetLength(0) + 1)
                throw new ArgumentOutOfRangeException("value", "The value is out of range.");
            _ertekek[a, b] = value;
            if(value == 2)
            {
                Maci j = new Maci();
                j.x = a;
                j.y = b;
                if(jarorszam == 0)
                {
                    _jarorok[0] = j;
                    ++jarorszam;
                }else if(jarorszam == 1)
                {
                    _jarorok[1] = j;
                    ++jarorszam;
                }
                else if(jarorszam == 2)
                {
                    _jarorok[2] = j;
                    ++jarorszam;
                }
                
            }
            if(value == 5)
            {
                m.x = a;
                m.y = b;
            }
        }

        public void SetValueAtJárőrLép(Int32 a, Int32 b, Int32 value)
        {
            if (a < 0 || a >= _ertekek.GetLength(0))
                throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
            if (b < 0 || b >= _ertekek.GetLength(1))
                throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");
            if (value < 0 || value > _ertekek.GetLength(0) + 1)
                throw new ArgumentOutOfRangeException("value", "The value is out of range.");
            _ertekek[a, b] = value;
        }




        public Int32 GetValue(Int32 x, Int32 y)
        {
            
            if (x < 0 || x >= _ertekek.GetLength(0))
                throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
            if (y < 0 || y >= _ertekek.GetLength(1))
                throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");
            return _ertekek[x, y];
        }
    }
}
