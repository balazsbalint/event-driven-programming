﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Beadandó.MaciLaci.Persistence
{
    public interface IMaciDataAccess
    {
        /// <summary>
        /// Fájl betöltése.
        /// </summary>
        /// <param name="path">Elérési útvonal.</param>
        /// <returns>A fájlból beolvasott játéktábla.</returns>
        Task<MaciTable> LoadAsync(string path, Int32 meret);

        /// <summary>
        /// Fájl mentése.
        /// </summary>
        /// <param name="path">Elérési útvonal.</param>
        /// <param name="table">A fájlba kiírandó játéktábla.</param>
        Task SaveAsync(string path, MaciTable table);

        Task<ICollection<SaveEntry>> ListAsync();
    }
}



    
      
 
