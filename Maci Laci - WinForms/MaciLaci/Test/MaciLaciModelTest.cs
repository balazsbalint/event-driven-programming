﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaciLaci.Program.Model;


namespace MaciLaci.Test
{
    [TestClass]
    public class ModelTest
    {
        private MaciModel _model;
        [TestMethod]
        public void Initialize()
        {
            _model = new MaciModel(9);
            _model._data = null;
        }

        [TestMethod]
        public void MaciLaciModelNewGame9()
        {
            _model = new MaciModel(9);

            Assert.AreEqual(_model.ModMéret, 9);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);
            Int32 kosarak = 0;
            Int32 járőrök = 0;
            for (Int32 i = 0; i < _model.ModMéret; i++)
                for (Int32 j = 0; j < _model.ModMéret; j++)
                {
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Kosár)
                        ++kosarak;
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Járőr)
                        ++járőrök;
                }
            Assert.AreEqual(kosarak, 8);
            Assert.AreEqual(járőrök, 1);

        }

        [TestMethod]
        public void MaciLaciModelNewGame10()
        {
            _model = new MaciModel(10);

            Assert.AreEqual(_model.ModMéret, 10);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);
            Int32 kosarak = 0;
            Int32 járőrök = 0;
            for (Int32 i = 0; i < _model.ModMéret; i++)
                for (Int32 j = 0; j < _model.ModMéret; j++) {
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Kosár)
                        ++kosarak;
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Járőr)
                        ++járőrök;
                }
            Assert.AreEqual(kosarak, 10);
            Assert.AreEqual(járőrök, 2);

        }

        [TestMethod]
        public void MaciLaciModelNewGame11()
        {
            _model = new MaciModel(11);

            Assert.AreEqual(_model.ModMéret, 11);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);
            Int32 kosarak = 0;
            Int32 járőrök = 0;
            for (Int32 i = 0; i < _model.ModMéret; i++)
                for (Int32 j = 0; j < _model.ModMéret; j++)
                {
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Kosár)
                        ++kosarak;
                    if ((FieldTypes)_model._table.GetValue(i, j) == FieldTypes.Járőr)
                        ++járőrök;
                }
            Assert.AreEqual(kosarak, 15);
            Assert.AreEqual(járőrök, 3);

        }

        [TestMethod]
        public void PauseTest()
        {
            _model = new MaciModel(9);
            _model.Pause = false;
            Assert.AreEqual(false, _model.Pause);
            _model.Pause = true;
            Assert.AreEqual(true, _model.Pause);
        }

        [TestMethod]
        public void LépésTeszt()
        {
            _model = new MaciModel(10);
            _model.MaciLép(Irány.Felfele);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);
            _model.MaciLép(Irány.Lefele);
            Assert.AreEqual(_model.GetErtek(1, 0), 5);
            _model.MaciLép(Irány.Felfele);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);
            _model.MaciLép(Irány.Jobbra);
            Assert.AreEqual(_model.GetErtek(0, 1), 5);
            _model.MaciLép(Irány.Balra);
            Assert.AreEqual(_model.GetErtek(0, 0), 5);

            _model = new MaciModel(9);
            Assert.AreEqual(_model.GetErtek(0, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(1, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(2, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(3, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(4, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(5, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(6, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(7, 8), 4);
            Assert.AreEqual(_model.GetErtek(6, 8), 2);
            _model.JárőrLéptet();
            Assert.AreEqual(_model.GetErtek(5, 8), 2);
        }
    }
}
