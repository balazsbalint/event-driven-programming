﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MaciLaci.Program.Persistence
{
    public class MaciFileDataAccess : IMaciDataAccess
    {
        public async Task<MaciTable> LoadAsync(String path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path)) // fájl megnyitása
                {
                    String line = await reader.ReadLineAsync();
                    Int32 tableSize = Int32.Parse(line); 
                    MaciTable table = new MaciTable(tableSize); // létrehozzuk a táblát
                    String[] numbers;
                    for (Int32 i = 0; i < tableSize; i++)
                    {
                        line = await reader.ReadLineAsync();
                        numbers = line.Split(' ');

                        for (Int32 j = 0; j < tableSize; j++)
                        {
                            table.SetValue(i, j, Int32.Parse(numbers[j]));
                        }
                    }
                    line = await reader.ReadLineAsync();
                    Int32 jaror = Int32.Parse(line);
                    table.jarorszam = jaror;
                    line = await reader.ReadLineAsync();
                    numbers = line.Split(' ');
                    for (int i = 0; i < jaror; i++)
                    {
                        if (numbers[i] == "False")
                        {
                            table.fordul[i] = false;
                        }
                        else
                        {
                            table.fordul[i] = true;
                        }
                    }
                    line = await reader.ReadLineAsync();
                    numbers = line.Split(' ');
                    table._óra = Int32.Parse(numbers[0]);
                    table._perc = Int32.Parse(numbers[1]);
                    table._másodperc = Int32.Parse(numbers[2]);
                    line = await reader.ReadLineAsync();
                    table.pause = Boolean.Parse(line);
                    line = await reader.ReadLineAsync();
                    table.KosárKell = Int32.Parse(line);
                    line = await reader.ReadLineAsync();
                    table.KosárGyűjt = Int32.Parse(line);
                    if(jaror == 3)
                    {
                        table.irányok = new int[3] { 0, 0, 1 };
                    }
                    return table;
                }
            }
            catch
            {
                throw new MaciDataException();
            }
        }

        public async Task SaveAsync(string path, MaciTable table)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path)) // fájl megnyitása
                {
                    writer.Write(table.Méret); // kiírjuk a méreteket
                    await writer.WriteLineAsync();
                    for (Int32 i = 0; i < table.Méret; i++)
                    {
                        for (Int32 j = 0; j < table.Méret; j++)
                        {
                            await writer.WriteAsync(table.GetValue(i, j) + " "); // kiírjuk az értékeket
                        }
                        await writer.WriteLineAsync();
                    }
                    await writer.WriteAsync(table.jarorszam + "");
                    await writer.WriteLineAsync();
                    for (int i = 0; i < table.irányok.Length; i++)
                    {
                        await writer.WriteAsync(table.fordul[i] + " ");
                    }
                    await writer.WriteLineAsync();
                    await writer.WriteAsync(table._óra + " " + table._perc + " " + table._másodperc);
                    await writer.WriteLineAsync();
                    await writer.WriteAsync(table.pause + "");
                    await writer.WriteLineAsync();
                    await writer.WriteAsync(table.KosárKell + "");
                    await writer.WriteLineAsync();
                    await writer.WriteAsync(table.KosárGyűjt + "");
                    
                }
            }
            catch
            {
                throw new MaciDataException();
            }
        }
    }
}
