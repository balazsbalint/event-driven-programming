﻿using System;
using MaciLaci.Program.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MaciLaci.Program.Model
{
    public enum FieldTypes { Üres, Kosár, Járőr, Fa, Kő, Maci }
    public enum Irány {Felfele, Lefele, Jobbra, Balra }

    public class MaciModel
    {
        public MaciTable _table;
        public IMaciDataAccess _data; 

        public event EventHandler<MaciEventArgs> GameOver;
        public Int32 getMp() { return _table._másodperc; }
        public Int32 getMinute() { return _table._perc; }
        public Int32 getÓra() { return _table._óra; }

        public void IdőEltelés()
        {
            ++_table._másodperc;
            if(_table._másodperc % 2 == 0)
            {
                JárőrLéptet();
            }
            if(_table._másodperc == 60)
            {
                _table._másodperc -= 60;
                ++_table._perc;
                if(_table._perc == 60)
                {
                    _table._perc -= 60;
                    ++_table._óra;
                }
            }
            
        }

        public async Task SaveGameAsync(String path)
        {
            if (_data == null)
                throw new InvalidOperationException("No data access is provided.");

            await _data.SaveAsync(path, _table);
        }

        public async Task LoadGameAsync(String path)
        {
            if (_data == null)
                throw new InvalidOperationException("No data access is provided.");

            _table = await _data.LoadAsync(path);
            
        }

        public MaciModel(Int32 value)
        {
            _table = new MaciTable(value);
            _data = new MaciFileDataAccess();
            PályaFeltölt();
        }

        public int ModMéret { get { return _table.Méret; } }

        public Int32 GetErtek(Int32 x, Int32 y)
        {
            if (x < 0 || x >= _table.Méret)
                throw new ArgumentOutOfRangeException("x", "The X coordinate is out of range.");
            if (y < 0 || y >= _table.Méret)
                throw new ArgumentOutOfRangeException("y", "The Y coordinate is out of range.");
            return _table.GetValue(x,y);
        }
        public void PályaFeltölt()
        {
            TextReader olvasó = new StreamReader("Persistence\\palya1.txt");
            if (_table.Méret == 9)
            {
                 olvasó = new StreamReader("Persistence\\palya1.txt");
                _table.KosárKell = 8;
                _table.KosárGyűjt = 0;
            }
            else if (_table.Méret == 10)
            {
                 olvasó = new StreamReader("Persistence\\palya2.txt");
                _table.KosárKell = 10;
                _table.KosárGyűjt = 0;
            }
            else if (_table.Méret == 11)
            {
                 olvasó = new StreamReader("Persistence\\palya3.txt");
                _table.KosárKell = 15;
                _table.KosárGyűjt = 0;
            }
            string sor;
            _table.SetValue(0, 0, 5);
            _table.SetMaci(0,0);
            for (int i = 0; i < 4; i++)
            {
                sor = olvasó.ReadLine();
                for (int j = 0; j < sor.Split(';').Length; j++)
                {
                    Int32 x, y;
                    if (sor.Split(';')[j].Split(',').Length == 2)
                    {
                        x = Int32.Parse(sor.Split(';')[j].Split(',')[0]);
                        y = Int32.Parse(sor.Split(';')[j].Split(',')[1]);
                    }
                    else {
                        x = Int32.Parse(((sor.Split(';')[j][0])).ToString());
                         y = Int32.Parse(((sor.Split(';')[j][1])).ToString());
                    }
                    
                    
                    if (i == 0)
                    {
                        _table.SetValue(x,y,2);
                    }
                    if(i == 1)
                    {
                        _table.SetValue(x, y, 1);
                    }
                    if (i == 2)
                    {
                        _table.SetValue(x, y, 3);
                    }
                    if (i == 3)
                    {
                        _table.SetValue(x, y, 4);
                    }
                   
                }
            }
            olvasó.Close();
            
        }
        public Int32 GetKosár()
        {
            return _table.KosárGyűjt;
        }

        public void MaciLép(Irány irany)
        {
            if (irany == Irány.Lefele)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if(x == _table.Méret -1)
                {

                }
                else if (_table.GetValue(x+1,y) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x + 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x + 1, y);
                }
                else if (_table.GetValue(x + 1, y) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x + 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x + 1, y);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
                
            }
            else if(irany == Irány.Felfele)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if(x == 0)
                {

                }
                else if (_table.GetValue(x -1, y) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x - 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x - 1, y);
                }
                else if (_table.GetValue(x - 1, y) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x - 1, y, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x - 1, y);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
            }
            else if(irany == Irány.Jobbra)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if (y == _table.Méret-1)
                {

                }
                else if (_table.GetValue(x, y+1) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x, y+1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y+1);
                }
                else if (_table.GetValue(x, y+1) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x , y + 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x , y + 1);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
            }
            else if (irany == Irány.Balra)
            {
                int x = _table.GetMaciX();
                int y = _table.GetMaciY();
                if (y == 0)
                {

                }
                else if (_table.GetValue(x, y - 1) == (int)FieldTypes.Üres)
                {
                    _table.SetValue(x, y - 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y - 1);
                }
                else if (_table.GetValue(x, y - 1) == (int)FieldTypes.Kosár)
                {
                    _table.SetValue(x, y - 1, (int)FieldTypes.Maci);
                    _table.SetValue(x, y, (int)FieldTypes.Üres);
                    _table.SetMaci(x, y - 1);
                    _table.KosárGyűjt = _table.KosárGyűjt + 1;
                    Console.WriteLine(_table.KosárGyűjt.ToString());
                }
            }
            if(_table.KosárGyűjt == _table.KosárKell)
            {
                GameOver(this, new MaciEventArgs(true, _table.KosárGyűjt, getÓra(), getMinute(), getMp()));
            }
            
        }

        public bool Pause
        {
            get { return _table.pause; }
            set { _table.pause = value; }

        }
        public void JárőrEllenőrzés()
        {
            for (int i = 0; i < _table._jarorok.Length; i++)
            {
                int x = _table._jarorok[i].x;
                int y = _table._jarorok[i].y;
                for (int j = Math.Max(0,x-1); j <Math.Min(_table.Méret,x+2) ; j++)
                {
                    for (int k = Math.Max(0, y - 1); k < Math.Min(_table.Méret, y + 2); k++)
                    {
                        if(_table.GetValue(j,k) == (int)FieldTypes.Maci)
                        {
                            GameOver(this, new MaciEventArgs(false,_table.KosárGyűjt,getÓra(),getMinute(),getMp()));
                        }
                    }
                }
            }
            
        }


        public void JárőrLéptet()
        {
            for (int i = 0; i < _table._jarorok.Length; i++)
            {
                if (_table.irányok[i] == 0)
                {
                    if (_table.fordul[i])
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (a == _table.Méret - 1)
                        {
                            _table.fordul[i] = false;
                        }
                        else if (_table.GetValue(a + 1, b) == 0)
                        {
                            _table.SetValueAtJárőrLép(a + 1, b, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].x = a + 1;
                        }
                        else if (_table.GetValue(a + 1, b) == 3 || _table.GetValue(a + 1, b) == 4)
                        {
                            _table.fordul[i] = false;
                        }
                    }
                    else
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (a == 0)
                        {
                            _table.fordul[i] = true;
                        }
                        else if (_table.GetValue(a - 1, b) == 0)
                        {
                            _table.SetValueAtJárőrLép(a - 1, b, 2);
                            _table.SetValueAtJárőrLép(a , b, 0);
                            _table._jarorok[i].x = a - 1;
                        }
                        else if (_table.GetValue(a - 1, b) == 3 || _table.GetValue(a - 1, b) == 4)
                        {
                            _table.fordul[i] = true;
                        }
                    }
                }
                else if (_table.irányok[i] == 1)
                {
                    if (_table.fordul[i])
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (b == _table.Méret - 1)
                        {
                            _table.fordul[i] = false;
                        }
                        else if (_table.GetValue(a, b + 1) == 0)
                        {
                            _table.SetValueAtJárőrLép(a, b + 1, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].y = b + 1;
                        }
                        else if (_table.GetValue(a, b + 1) == 3 || _table.GetValue(a, b + 1) == 4)
                        {
                            _table.fordul[i] = false;
                        }
                    }
                    else
                    {
                        var a = _table._jarorok[i].x;
                        var b = _table._jarorok[i].y;
                        if (b == 0)
                        {
                            _table.fordul[i] = true;
                        }
                        else if (_table.GetValue(a, b - 1) == 0)
                        {
                            _table.SetValueAtJárőrLép(a, b-1, 2);
                            _table.SetValueAtJárőrLép(a, b, 0);
                            _table._jarorok[i].y = b - 1;
                        }
                        else if (_table.GetValue(a, b - 1) == 3 || _table.GetValue(a, b - 1) == 4)
                        {
                            _table.fordul[i] = true;
                        }
                    }
                }
            }
            
        }

    }
}
