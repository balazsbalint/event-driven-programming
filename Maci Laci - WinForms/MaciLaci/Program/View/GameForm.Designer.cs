﻿namespace MaciLaci.Program
{
    partial class JatekTabla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameSet = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nwg = new System.Windows.Forms.Button();
            this.svgame = new System.Windows.Forms.Button();
            this.ldgame = new System.Windows.Forms.Button();
            this.pause = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.PiknikName = new System.Windows.Forms.ToolStripStatusLabel();
            this.pikniklbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timelbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.gombtartó = new System.Windows.Forms.Panel();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gameSet)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameSet
            // 
            this.gameSet.AutoSize = false;
            this.gameSet.BackColor = System.Drawing.Color.Black;
            this.gameSet.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.gameSet.LargeChange = 1;
            this.gameSet.Location = new System.Drawing.Point(22, 12);
            this.gameSet.Maximum = 3;
            this.gameSet.Minimum = 1;
            this.gameSet.Name = "gameSet";
            this.gameSet.Size = new System.Drawing.Size(215, 45);
            this.gameSet.TabIndex = 0;
            this.gameSet.Value = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(18, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "9 x 9";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(100, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "10 x 10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(185, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "11 x 11";
            // 
            // nwg
            // 
            this.nwg.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nwg.Location = new System.Drawing.Point(258, 13);
            this.nwg.Name = "nwg";
            this.nwg.Size = new System.Drawing.Size(91, 52);
            this.nwg.TabIndex = 4;
            this.nwg.Text = "Új játék";
            this.nwg.UseVisualStyleBackColor = true;
            this.nwg.Click += new System.EventHandler(this.nwg_Click);
            // 
            // svgame
            // 
            this.svgame.Enabled = false;
            this.svgame.Font = new System.Drawing.Font("MingLiU-ExtB", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.svgame.Location = new System.Drawing.Point(355, 42);
            this.svgame.Name = "svgame";
            this.svgame.Size = new System.Drawing.Size(132, 23);
            this.svgame.TabIndex = 5;
            this.svgame.Text = "Mentés";
            this.svgame.UseVisualStyleBackColor = true;
            this.svgame.Click += new System.EventHandler(this.svgame_Click);
            // 
            // ldgame
            // 
            this.ldgame.Enabled = false;
            this.ldgame.Font = new System.Drawing.Font("MingLiU-ExtB", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ldgame.Location = new System.Drawing.Point(355, 13);
            this.ldgame.Name = "ldgame";
            this.ldgame.Size = new System.Drawing.Size(132, 23);
            this.ldgame.TabIndex = 6;
            this.ldgame.Text = "Betöltés";
            this.ldgame.UseVisualStyleBackColor = true;
            this.ldgame.Click += new System.EventHandler(this.ldgame_Click);
            // 
            // pause
            // 
            this.pause.Enabled = false;
            this.pause.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pause.Location = new System.Drawing.Point(599, 24);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(83, 31);
            this.pause.TabIndex = 7;
            this.pause.Text = "Szünet";
            this.pause.UseVisualStyleBackColor = true;
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PiknikName,
            this.pikniklbl,
            this.toolStripStatusLabel1,
            this.timelbl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(734, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PiknikName
            // 
            this.PiknikName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.PiknikName.Name = "PiknikName";
            this.PiknikName.Size = new System.Drawing.Size(121, 17);
            this.PiknikName.Text = "Piknikkosarak száma: ";
            // 
            // pikniklbl
            // 
            this.pikniklbl.Name = "pikniklbl";
            this.pikniklbl.Size = new System.Drawing.Size(17, 17);
            this.pikniklbl.Text = "--";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel1.Text = "Eltelt idő: ";
            // 
            // timelbl
            // 
            this.timelbl.Name = "timelbl";
            this.timelbl.Size = new System.Drawing.Size(17, 17);
            this.timelbl.Text = "--";
            // 
            // gombtartó
            // 
            this.gombtartó.Location = new System.Drawing.Point(12, 102);
            this.gombtartó.Name = "gombtartó";
            this.gombtartó.Size = new System.Drawing.Size(710, 524);
            this.gombtartó.TabIndex = 9;
            // 
            // _saveFileDialog
            // 
            this._saveFileDialog.Filter = "Maci laci tábla (*.stl)|*.stl";
            this._saveFileDialog.Title = "Maci Laci játék mentése";
            // 
            // _openFileDialog
            // 
            this._openFileDialog.FileName = "Maci laci tábla (*.stl)|*.stl";
            this._openFileDialog.Title = "Maci Laci játék betöltése";
            // 
            // JatekTabla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(734, 661);
            this.Controls.Add(this.gombtartó);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.ldgame);
            this.Controls.Add(this.svgame);
            this.Controls.Add(this.nwg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gameSet);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JatekTabla";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maci Laci";
            ((System.ComponentModel.ISupportInitialize)(this.gameSet)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar gameSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button nwg;
        private System.Windows.Forms.Button svgame;
        private System.Windows.Forms.Button ldgame;
        private System.Windows.Forms.Button pause;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel PiknikName;
        private System.Windows.Forms.ToolStripStatusLabel pikniklbl;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel timelbl;
        private System.Windows.Forms.Panel gombtartó;
        private System.Windows.Forms.SaveFileDialog _saveFileDialog;
        private System.Windows.Forms.OpenFileDialog _openFileDialog;
    }
}

