﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaciLaci.Program.Model;
using MaciLaci.Program.Persistence;

namespace MaciLaci.Program
{
    public partial class JatekTabla : Form
    {
        private MaciModel _model;
        private Button[,] gombok;
        private bool canpress;
        private bool timerindult;
        private Timer _timer;
        void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (canpress)
            {
                if (e.KeyChar == 'w')
                {
                    _model.MaciLép(Irány.Felfele);
                    updateButton();
                    
                }
                if (e.KeyChar == 's')
                {
                    _model.MaciLép(Irány.Lefele);
                    updateButton();
                }
                if (e.KeyChar == 'a')
                {
                    _model.MaciLép(Irány.Balra);
                    updateButton();
                }
                if (e.KeyChar == 'd')
                {
                    _model.MaciLép(Irány.Jobbra);
                    updateButton();
                }
                _model.JárőrEllenőrzés();
            }
        }

        public JatekTabla()
        {
            InitializeComponent();
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(Form1_KeyPress);
            canpress = false;
            timerindult = false;           
        }
        private void Game_GameOver(Object sender, MaciEventArgs e)
        {
            _timer.Stop();
            if (e.IsWon)
            {
                if(MessageBox.Show("Gratulálok, győztél!" + Environment.NewLine +
                                "Összesen " + e.Kosár + " kosárt gyűjtöttél és " +
                                e.Óra + ":" + e.Perc + ":" + e.Másodperc + " ideig játszottál." + Environment.NewLine +"Szeretnél új játékot?",
                                "Maci Laci játék",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information)== DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    Application.Exit();
                }
            }
            else {
                if(MessageBox.Show("Sajnálom, vesztettél, meglátott egy járőr!" + Environment.NewLine + "Szeretnél új játékot?",
                                "Maci Laci játék",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    Application.Exit();
                }
            };
        }
        private void update()
        {
            int dx = gombtartó.Width / _model.ModMéret;
            int dy = gombtartó.Height / _model.ModMéret;
            gombtartó.Controls.Clear();
            for (int i = 0; i < _model.ModMéret; i++)
            {
                for (int j = 0; j < _model.ModMéret; j++)
                {
                    var gomb = new Button();
                    gomb.Location = new Point(j * dx, i * dy);
                    gomb.Size = new Size(dx, dy);
                    if (_model.GetErtek(i, j) == 5)
                    {
                        gomb.Image = Image.FromFile(@"Persistence\img\kep.png");
                        gomb.ImageAlign = ContentAlignment.MiddleCenter;
                        gomb.TextAlign = ContentAlignment.MiddleRight;
                        gomb.BackColor = Color.GhostWhite;
                    }
                    else if (_model.GetErtek(i, j) == 1)
                    {
                        gomb.Image = Image.FromFile(@"Persistence\img\kep2.png");
                        gomb.ImageAlign = ContentAlignment.MiddleCenter;
                        gomb.TextAlign = ContentAlignment.MiddleRight;
                        gomb.BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 2)
                    {
                        gomb.Image = Image.FromFile(@"Persistence\img\kep3.png");
                        gomb.ImageAlign = ContentAlignment.MiddleCenter;
                        gomb.TextAlign = ContentAlignment.MiddleRight;
                        gomb.BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 3)
                    {
                        gomb.Image = Image.FromFile(@"Persistence\img\kep5.png");
                        gomb.ImageAlign = ContentAlignment.MiddleCenter;
                        gomb.TextAlign = ContentAlignment.MiddleRight;
                        gomb.BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 4)
                    {
                        gomb.Image = Image.FromFile(@"Persistence\img\kep4.png");
                        gomb.ImageAlign = ContentAlignment.MiddleCenter;
                        gomb.TextAlign = ContentAlignment.MiddleRight;
                        gomb.BackColor = Color.White;
                    }
                    else
                    {
                        gomb.BackColor = Color.LightGreen;
                    }
                    gombok[i, j] = gomb;
                    gombtartó.Controls.Add(gomb);
                }
            }
            pikniklbl.Text = _model.GetKosár().ToString();
            timelbl.Text = _model.getÓra().ToString() + ":" + _model.getMinute().ToString() + ":" + _model.getMp().ToString();
        }
        private void Időzítő(Object sender, EventArgs e)
        {
            updateButton();
            _model.IdőEltelés();
            
            if((_model.getMp() % 2) == 0)
            {
                timelbl.Text = _model.getÓra().ToString() + ":" + _model.getMinute().ToString() + ":" + _model.getMp().ToString();
                updateButton();
            }
            else
            {
                timelbl.Text = _model.getÓra().ToString() + ":" + _model.getMinute().ToString() + ":" + _model.getMp().ToString();
            }
            _model.JárőrEllenőrzés();

        }
        private void nwg_Click(object sender, EventArgs e)
        {
            
            _timer = new Timer();
            if (!timerindult)
            {
                _timer.Interval = 1000;
                _timer.Tick += new EventHandler(Időzítő);
                _timer.Start();
                timerindult = true;
            }
            
            canpress = true;
            if (gameSet.Value == 1)
            {
                _model = new MaciModel(9);
            }else if(gameSet.Value == 2)
            {
                _model = new MaciModel(10);
            }
            else if (gameSet.Value == 3)
            {
                _model = new MaciModel(11);
            }
            gombok = new Button[_model.ModMéret,_model.ModMéret];
            svgame.Enabled = true;
            ldgame.Enabled = true;
            pause.Enabled = true;
            _model.GameOver += new EventHandler<MaciEventArgs>(Game_GameOver);
            _model.Pause = false;
            update();
            
            
            
        }

        private void JatekTabla_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void updateButton()
        {
            for (int i = 0; i < _model.ModMéret; i++)
            {
                for (int j = 0; j < _model.ModMéret; j++)
                {
                    if (_model.GetErtek(i, j) == 5)
                    {
                        gombok[i,j].Image = Image.FromFile(@"Persistence\img\kep.png");
                        gombok[i, j].ImageAlign = ContentAlignment.MiddleCenter;
                        gombok[i, j].TextAlign = ContentAlignment.MiddleRight;
                        gombok[i, j].BackColor = Color.GhostWhite;
                    }
                    else if (_model.GetErtek(i, j) == 1)
                    {
                        gombok[i, j].Image = Image.FromFile(@"Persistence\img\kep2.png");
                        gombok[i, j].ImageAlign = ContentAlignment.MiddleCenter;
                        gombok[i, j].TextAlign = ContentAlignment.MiddleRight;
                        gombok[i, j].BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 2)
                    {
                        gombok[i, j].Image = Image.FromFile(@"Persistence\img\kep3.png");
                        gombok[i, j].ImageAlign = ContentAlignment.MiddleCenter;
                        gombok[i, j].TextAlign = ContentAlignment.MiddleRight;
                        gombok[i, j].BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 3)
                    {
                        gombok[i, j].Image = Image.FromFile(@"Persistence\img\kep5.png");
                        gombok[i, j].ImageAlign = ContentAlignment.MiddleCenter;
                        gombok[i, j].TextAlign = ContentAlignment.MiddleRight;
                        gombok[i, j].BackColor = Color.White;
                    }
                    else if (_model.GetErtek(i, j) == 4)
                    {
                        gombok[i, j].Image = Image.FromFile(@"Persistence\img\kep4.png");
                        gombok[i, j].ImageAlign = ContentAlignment.MiddleCenter;
                        gombok[i, j].TextAlign = ContentAlignment.MiddleRight;
                        gombok[i, j].BackColor = Color.White;
                    }
                    else
                    {
                        gombok[i, j].Image = Image.FromFile(@"Persistence\img\ures.png");
                        gombok[i, j].BackColor = Color.LightGreen;
                    }
                }
            }
            pikniklbl.Text = "" + _model.GetKosár();
        }

        private void pause_Click(object sender, EventArgs e)
        {
            if (!_model.Pause)
            {
                _model.Pause = true;
                canpress = false;
                _timer.Stop();
            }
            else
            {
                canpress = true;
                _model.Pause = false;
                _timer.Start();
            }
        }

        private async void svgame_Click(object sender, EventArgs e)
        {
            Boolean restartTimer = _timer.Enabled;
            _timer.Stop();

            if (_saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // játé mentése
                    await _model.SaveGameAsync(_saveFileDialog.FileName);
                }
                catch (MaciDataException)
                {
                    MessageBox.Show("Játék mentése sikertelen!" + Environment.NewLine + "Hibás az elérési út, vagy a könyvtár nem írható.", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (restartTimer)
                _timer.Start();
        }

        private async void ldgame_Click(object sender, EventArgs e)
        {
            Boolean restartTimer = _timer.Enabled;
            _timer.Stop();

            if (_openFileDialog.ShowDialog() == DialogResult.OK) // ha kiválasztottunk egy fájlt
            {
                try
                {
                    // játék betöltése
                    await _model.LoadGameAsync(_openFileDialog.FileName);
                    gombok = new Button[_model.ModMéret, _model.ModMéret];
                }
                catch (MaciDataException)
                {
                    MessageBox.Show("Játék betöltése sikertelen!" + Environment.NewLine + "Hibás az elérési út, vagy a fájlformátum.", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    _model.PályaFeltölt();
                }
                
                update();
            }

            if (restartTimer)
                _timer.Start();
        }
    }
}
