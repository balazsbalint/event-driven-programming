QT       += testlib

QT       -= gui

TARGET = gomodeltest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    gomodel.cpp \
    gomodeltest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    goadatkezelo.h \
    goadatkezelomock.h \
    gomodel.h
