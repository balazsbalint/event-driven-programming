#include "gomodel.h"


GoModel::GoModel(GoAdatKezelo* ertek)
{
    _tableSize = 0;
    _adat = ertek;
}

GoModel::Player GoModel::_data(int a, int b) const
{
    return _table[a][b];
}

void GoModel::setSize(int value)
{
    _tableSize = value;
}

bool GoModel::loadIn(int gameIndex)
{
    QVector<int> mentettAdatok;
    if(!_adat->loadGame(gameIndex, mentettAdatok)) return false;
    _tableSize = mentettAdatok[0];
    _roundcount = mentettAdatok[1];
    _ScoreB = mentettAdatok[2];
    _ScoreW = mentettAdatok[3];
    if(mentettAdatok[4] == 1)
    {
        _current = PlayerB;
    }else _current = PlayerW;
    _table.clear();
    _table.resize(_tableSize);
    for(int i=0; i< _table.size(); i++)
    {
        _table[i].resize(_table.size());
    }
    for (int i = 0; i < _tableSize; ++i){
           for (int j = 0; j < _tableSize; ++j)
           {
               _table[i][j] = (Player)mentettAdatok[5 + i * _tableSize + j];
           }
    }
    return true;
}

bool GoModel::saveOut(int gameIndex)
{
    QVector<int> menteniValo;
    menteniValo.push_back(_tableSize);
    menteniValo.push_back(_roundcount);
    menteniValo.push_back(_ScoreB);
    menteniValo.push_back(_ScoreW);
    menteniValo.push_back((int)_current);
    for (int i = 0; i < _tableSize; ++i){
           for (int j = 0; j < _tableSize; ++j)
           {
               menteniValo.push_back((int)_table[i][j]);
           }
    }
    return _adat->saveGame(gameIndex, menteniValo);
}

QVector<QString> GoModel::saveGameList() const
{
    return _adat->saveGameList();
}

void GoModel::newGame()
{
    if(_table.size()>0) _table.clear();
    _table.resize(_tableSize);
    for(int i=0; i< _table.size(); i++)
    {
        _table[i].resize(_table.size());
        for(int j = 0; j< _tableSize; j++)
        {
            _table[i][j] =  Player0;
        }
    }
    _current = PlayerB;
    _roundcount = 0;
    _ScoreB = 0;
    _ScoreW = 0;
}

void GoModel::checkStones()
{
    for(int i=0; i<_tableSize; ++i)
    {
        for(int j= 0; j < _tableSize; ++j)
        {
            if(_table[i][j] !=0)
            {
                Player check = Player0;
                if(_table[i][j] == PlayerB){
                    check = PlayerW;
                }else check = PlayerB;

                if(i ==0 && j==0)
                {
                    if(_table[0][1] == check && _table[1][0] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i==(_tableSize-1) && j ==(_tableSize-1))
                {
                    if(_table[i-1][j] == check && _table[i][j-1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i==0 && j==(_tableSize-1))
                {
                    if(_table[i+1][j] == check && _table[i][j-1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i==(_tableSize-1) && j==0)
                {
                    if(_table[i-1][j] == check && _table[i][j+1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i==0 && j>0 && j<(_tableSize-1))
                {
                    if(_table[i+1][j] == check && _table[i][j+1] == check && _table[i][j-1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(j==0 && i>0 && i<(_tableSize-1))
                {
                    if(_table[i+1][j] == check && _table[i][j+1] == check && _table[i-1][j] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i==(_tableSize-1) && j>0 && j<(_tableSize-1))
                {
                    if(_table[i-1][j] == check && _table[i][j+1] == check && _table[i][j-1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(j==(_tableSize-1) && i>0 && i<(_tableSize-1))
                {
                    if(_table[i-1][j] == check && _table[i][j-1] == check && _table[i+1][j] == check)
                    {
                        removeStones(i,j,check);
                    }
                }else if(i>0 && i<(_tableSize-1) && j>0 && j<(_tableSize-1))
                {
                    if(_table[i-1][j] == check && _table[i][j+1] == check && _table[i+1][j] == check && _table[i][j-1] == check)
                    {
                        removeStones(i,j,check);
                    }
                }
            }
        }
    }
}


void GoModel::removeStones(int i, int j, Player check)
{
    _table[i][j] = Player0;
    colourBack(i, j);
    if(check == PlayerB){
        ++_ScoreB;
    }else ++_ScoreW;
}

 void GoModel::round(int a, int b)
 {
     if(a >= 0 && a <_tableSize  && b >=0 && b<_tableSize)
     {
         if(_table[a][b] == Player0)
         {
            _table[a][b] = _current;
            roundDone(a,b, _current);
            if(_current == PlayerB)
            {
                _current = PlayerW;
            }else if(_current == PlayerW)
            {
                _current = PlayerB;
                ++_roundcount;
            }
            checkStones();
            isEnd();
         }
     }

 }

 void GoModel::isEnd()
 {
     if(_roundcount == _tableSize)
     {
         if(_ScoreB == _ScoreW)
         {
             gameDraw();
         }else if(_ScoreB > _ScoreW)
         {
             gameWon(PlayerB);
         }else gameWon(PlayerW);
     }
 }

GoModel::~GoModel()
{
    _table.clear();
}
