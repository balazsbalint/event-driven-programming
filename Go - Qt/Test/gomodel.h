#ifndef GOMODEL_H
#define GOMODEL_H

#include <QObject>
#include "goadatkezelo.h"
#include <vector>
#include <QVector>

class GoModel : public QObject
{
    Q_OBJECT
public:
    GoModel(GoAdatKezelo* ertek);
    void setSize(int value);
    virtual ~GoModel();
    enum Player { Player0, PlayerB, PlayerW };
    void newGame();
    void round(int a, int b);
    void removeStones(int i, int j, Player check);
    void checkStones();
    void isEnd();
    bool loadIn(int gameIndex);
    bool saveOut(int gameIndex);
    QVector<QString> saveGameList() const;
    Player currentPlayer(){ return _current;}
    int getSize() { return _tableSize;}
    int getRoundCount() { return  _roundcount;}
    int getScoreB() { return _ScoreB;}
    int getScoreW() { return _ScoreW;}
    Player _data(int a, int b) const;


signals:
    void gameWon(GoModel::Player player);
    void gameDraw();
    void roundDone(int x, int y, GoModel::Player player);
    void colourBack(int x, int y);
private:
    int _tableSize;
    int _roundcount;
    int _ScoreB;
    int _ScoreW;
    Player _current;
    GoAdatKezelo* _adat;
    QVector<QVector<Player> > _table;
};

#endif // GOMODEL_H
