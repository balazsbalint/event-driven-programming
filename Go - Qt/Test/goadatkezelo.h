#ifndef GOADATKEZELO_H
#define GOADATKEZELO_H

#include <QString>
#include <QVector>

class GoAdatKezelo
{
public:
    GoAdatKezelo(){}
    virtual ~GoAdatKezelo() {}

    virtual bool isAvailable() const { return false; }
    virtual bool loadGame(int gameIndex, QVector<int> &saveGameData) = 0;
    virtual bool saveGame(int gameIndex, const QVector<int> &saveGameData) = 0;

    virtual QVector<QString> saveGameList() const = 0;
};

#endif // GOADATKEZELO_H
