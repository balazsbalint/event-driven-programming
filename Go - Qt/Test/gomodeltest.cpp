#include <QString>
#include <QtTest>
#include "gomodel.h"
#include "goadatkezelomock.h"

class GoModelTest : public QObject // tesztkörnyezet
{
    Q_OBJECT
private:
    GoModel* _model;
    GoAdatKezelo *_dataAccess;

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testNewGame5();
    void testNewGame9();
    void testNewGame19();
    void testRound();
    void testWrongSteps();
    void testMentes();
    void testBetoltes();
};

void GoModelTest::initTestCase()
{
    _dataAccess = new GoAdatKezeloMock();
    _model = new GoModel(_dataAccess);
}

void GoModelTest::cleanupTestCase()
{
    delete _dataAccess;
    delete _model;
}


void GoModelTest::testNewGame5()
{
    _model->setSize(5);
    _model->newGame();
    QCOMPARE(_model->getSize(), 5);
    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    for(int i= 0; i< 5; ++i)
    {
        for(int j= 0; j< 5; ++j){
             QCOMPARE(_model->_data(i,j), GoModel::Player0);
        }
    }
}

void GoModelTest::testNewGame9()
{
    _model->setSize(9);
    _model->newGame();
    QCOMPARE(_model->getSize(), 9);
    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    for(int i= 0; i< 9; ++i)
    {
        for(int j= 0; j< 9; ++j){
            QCOMPARE(_model->_data(i,j), GoModel::Player0);
        }
    }
}

void GoModelTest::testNewGame19()
{
    _model->setSize(19);
    _model->newGame();
    QCOMPARE(_model->getSize(), 19);
    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    for(int i= 0; i< 19; ++i)
    {
        for(int j= 0; j< 19; ++j){
            QCOMPARE(_model->_data(i,j), GoModel::Player0);
        }
    }
}

void GoModelTest::testRound()
{
    _model->setSize(5);
    _model->newGame();
    _model->round(0,0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerW);
    QCOMPARE(_model->_data(0,0), GoModel::PlayerB);
    QCOMPARE(_model->getRoundCount(), 0);
    for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
                QVERIFY((i == 0 && j == 0) || (_model->_data(i, j) == GoModel::Player0));
    _model->round(0,1);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    QCOMPARE(_model->_data(0,1), GoModel::PlayerW);
    QCOMPARE(_model->getRoundCount(), 1);
    //lépünk mégegyet a feketével, majd a fehérrel úgy, hogy lekerüljön egy fekete
    _model->round(0,2);
    _model->round(1,0);
    //megnézzük lekerült e a kő
    QCOMPARE(_model->_data(0,0), GoModel::Player0);
    //megnézzük, hogy változott e a fehérek pontja
    QCOMPARE(_model->getScoreW(), 1);
    QCOMPARE(_model->getScoreB(), 0);

    //nézzük meg, hogy fordítva is működik e, a fekete pontja is nő e majd
    _model->setSize(9);
    _model->newGame();
    //négy oldalról fogja ezt körbevenni a fekete
    _model->round(3,3);
    _model->round(4,3);
    _model->round(4,4);
    _model->round(0,0);
    _model->round(4,2);
    _model->round(6,6);
    _model->round(5,3);
    _model->round(6,8);
    QCOMPARE(_model->getScoreW(), 0);
    QCOMPARE(_model->getScoreB(), 1);
    //nézzük meg hanyadik körben vagyunk
    QCOMPARE(_model->getRoundCount(), 4);
    //nézzük meg ki fog következni
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    //végül nézzük meg, hogy szabad lett, e a mező
    QCOMPARE(_model->_data(4,3), GoModel::Player0);
    //azt is megnézhetjük, hogy az előzőeken kívül a többi mező is üres-e
    for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                QVERIFY((i == 3 && j == 3)  ||(i == 4 && j == 4)
                        || (i == 0 && j == 0)  || (i == 4 && j == 2)
                        || (i == 6 && j == 6)  || (i == 5 && j == 3)
                        || (i == 6 && j == 8)  || (_model->_data(i, j) == GoModel::Player0));

}

void GoModelTest::testWrongSteps()
{
    _model->setSize(19);
    _model->newGame();

    _model->round(-50, 0);
    _model->round(0, -80);
    _model->round(4, 100);
    _model->round(100, 4);
    _model->round(1000, 1000);
    _model->round(-1000,-1000);
    _model->round(-1000, 1000);
    _model->round(1000, -1000);

    QCOMPARE(_model->getRoundCount(), 0);
    for (int i = 0; i < 19; i++)
    {
        for (int j = 0; j < 19; j++)
        {
            QVERIFY(_model->_data(i, j) == GoModel::Player0);
        }
    }

    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    QCOMPARE(_model->getScoreW(), 0);
    QCOMPARE(_model->getScoreB(), 0);
    _model->round(6,6);
    QCOMPARE(_model->_data(6,6), GoModel::PlayerB);
    _model->round(6,6);
    QCOMPARE(_model->_data(6,6), GoModel::PlayerB);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerW);
}

void GoModelTest::testMentes()
{
    _model->setSize(5);
    _model->newGame();

    _model->round(3,3);
    _model->round(2,1);
    _model->round(1,3);
    _model->round(0,4);
    _model->round(4,0);
    _model->round(2,2);

    _model->saveOut(0);
}

void GoModelTest::testBetoltes()
{
    _model->setSize(9);
    _model->newGame();

    _model->round(3,3);
    _model->round(2,1);
    _model->round(1,3);
    _model->round(0,4);
    _model->round(4,0);
    _model->round(2,2);

    _model->loadIn(9);

    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(),0);
    for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                QVERIFY((_model->_data(i, j) == GoModel::Player0));

    _model->setSize(5);
    _model->newGame();

    _model->round(3,3);
    _model->round(2,1);
    _model->round(1,3);
    _model->round(0,4);
    _model->round(4,0);
    _model->round(2,2);

    _model->loadIn(5);
    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(),0);
    for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
                QVERIFY((_model->_data(i, j) == GoModel::Player0));

    _model->setSize(19);
    _model->newGame();

    _model->round(3,3);
    _model->round(2,1);
    _model->round(1,3);
    _model->round(0,4);
    _model->round(4,0);
    _model->round(2,2);

    _model->loadIn(19);
    QCOMPARE(_model->getRoundCount(), 0);
    QCOMPARE(_model->currentPlayer(), GoModel::PlayerB);
    QCOMPARE(_model->getScoreB(), 0);
    QCOMPARE(_model->getScoreW(),0);
    for (int i = 0; i < 19; i++)
            for (int j = 0; j < 19; j++)
                QVERIFY((_model->_data(i, j) == GoModel::Player0));
    QCOMPARE(_model->loadIn(0), false);
    QCOMPARE(_model->loadIn(-220), false);
    QCOMPARE(_model->loadIn(1500), false);

}

QTEST_APPLESS_MAIN(GoModelTest)

#include "gomodeltest.moc"
