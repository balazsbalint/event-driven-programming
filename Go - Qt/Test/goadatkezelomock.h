#ifndef TICTACTOEDATAACCESSMOCK_H
#define TICTACTOEDATAACCESSMOCK_H

#include "goadatkezelo.h"

class GoAdatKezeloMock : public GoAdatKezelo
{
    bool isAvailable() const {return true;}
    QVector<QString > saveGameList() const
    {
        return  QVector<QString>(5);
    }

    bool saveGame(int gameIndex, const QVector<int> &saveGameData)
    {
        qDebug() << "A jatek elmentve az  (" << gameIndex << "). helyre ezekkel az értékekkel: ";

        for (int i = 0; i < (saveGameData.size()) ; ++i){
                qDebug() << saveGameData[i] << " ";
        }
        qDebug() << endl;

        return true;
    }

    bool loadGame(int gameIndex, QVector<int> &saveGameData) //itt a gameIndex fogja megmondani mekkora lesz a vektor
    {
        int meret;
        if(gameIndex == 5) meret = 30;
        else if(gameIndex == 9) meret = 86;
        else if(gameIndex == 19) meret = 366;
        else return false;
        saveGameData.resize(meret);
        qDebug() << "A játék betöltve a (" << gameIndex << ") -es mérettel ezekkel az értékekkel: ";

        for (int i = 0; i < meret; i++)
                    qDebug() << saveGameData[i] << " ";
        saveGameData[4] = 1;
        if(gameIndex == 5) saveGameData[0] = 5;
        else if(gameIndex == 9) saveGameData[0] = 9;
        else if(gameIndex == 19) saveGameData[0] = 19;
        qDebug() << endl;

        return true;
    }

    
};

#endif // TICTACTOEDATAACCESSMOCK_H
