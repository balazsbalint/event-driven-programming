#include "goadatkezelo.h"

QVector<QString> GoAdatKezelo::saveGameList() const
{
    QVector<QString> result(5);
    for(int i=0; i<5; ++i)
    {
        if (QFile::exists("game" + QString::number(i) + ".sav"))
        {
            QFileInfo info("game"+ QString::number(i) + ".sav");
            result[i] = "[" + QString::number(i + 1) + "] " + info.lastModified().toString("yyyy.MM.dd HH:mm:ss");
        }
    }

    return result;
}


bool GoAdatKezelo::saveGame(int gameIndex, const QVector<int> &saveGameData)
{
    QFile file("game" + QString::number(gameIndex) + ".sav");
    if (!file.open(QFile::WriteOnly)) return false;

    QTextStream stream(&file);
    for (int i = 0; i < (saveGameData.size()) ; ++i){
            stream << saveGameData[i] << endl;
    }
    file.close();
    return true;
}

bool GoAdatKezelo::loadGame(int gameIndex, QVector<int> &saveGameData)
{
    QFile file("game" + QString::number(gameIndex) + ".sav");
    if (!file.open(QFile::ReadOnly)) return false;

    QTextStream stream(&file);
    int nagys = stream.readLine().toInt();
    int meret = (nagys*nagys)+4;
    //saveGameData.resize(meret);
    saveGameData.push_back(nagys);
    for (int i = 1; i < meret+1; ++i){
            saveGameData.push_back(stream.readLine().toInt());
    }
    file.close();
    return true;
}
