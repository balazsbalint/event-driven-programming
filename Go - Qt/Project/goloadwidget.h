#ifndef GOLOADWIDGET_H
#define GOLOADWIDGET_H
#include "gosavewidget.h"

class GoLoadWidget : public GoSaveWidget
{
    Q_OBJECT
public:
    explicit GoLoadWidget(QWidget *parent = 0);
protected slots:
    void okButton_Clicked();
};

#endif // GOLOADWIDGET_H
