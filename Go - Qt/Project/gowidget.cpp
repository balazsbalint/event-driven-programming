#include "gowidget.h"
#include "ui_gowidget.h"
#include <QKeyEvent>

GoWidget::GoWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::GoWidget)
{
    ui->setupUi(this);
    connect(ui->_newGameButton,SIGNAL(clicked()),this,SLOT(nwgmbt_clicked()));
    setMinimumSize(536,636);
    setWindowTitle(tr("Go!"));
    connect(&_model, SIGNAL(roundDone(int, int, GoModel::Player)), this, SLOT(_model_round_done(int, int, GoModel::Player)));
    connect(&_model, SIGNAL(colourBack(int, int)), this, SLOT(_model_colour_back(int, int)));
    connect(&_model, SIGNAL(gameDraw()), this, SLOT(_model_gameDraw()));
    connect(&_model, SIGNAL( gameWon(GoModel::Player)), this, SLOT(_model_gameWon(GoModel::Player)));
    btnumber =0;
}

void GoWidget::_model_gameWon(GoModel::Player Player)
{
    if(Player == 1)
    {
        QMessageBox::StandardButton reply = QMessageBox::information(this,trUtf8("Végeredmény"), trUtf8("A játékot a fekete kövek játékosa nyerte!  Szeretnél új játékot?"), QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
        {
            QApplication::quit();
        }else
        {
            qApp->quit();
            QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
        }
    }else {
        QMessageBox::StandardButton reply = QMessageBox::information(this,trUtf8("Végeredmény"), trUtf8("A játékot a fehér kövek játékosa nyerte! Szeretnél új játékot?"), QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
        {
            QApplication::quit();
        }else
        {
            qApp->quit();
            QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
        }
    }
}

void GoWidget::_model_gameDraw()
{
    QMessageBox::StandardButton reply = QMessageBox::information(this,trUtf8("Végeredmény"), tr("Döntetlen lett a meccs! Szeretnél új játékot?"), QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::No)
    {
        QApplication::quit();
    }else
    {
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}


void GoWidget::_model_colour_back(int a, int b)
{
     btnholder[a][b]->setIcon(QIcon(":/images/stonedflt.png"));
}

void GoWidget::_model_round_done(int a, int b, GoModel::Player Player)
{
    if(Player == 1)
    {
        btnholder[a][b]->setIcon(QIcon(":/images/stone1.png"));
    }else{

        btnholder[a][b]->setIcon(QIcon(":/images/stone2.png"));
    }
}

void GoWidget::nwgmbt_clicked()
{
    if(ui->rbt5->isChecked())
    {
        btnumber = 5;
        setFixedSize(610,680);
        ui->gombok->setFixedSize(530,420);
        ui->tarto->setFixedSize(530, 101);
        _model.setSize(5);
    }else if(ui->rbt9->isChecked()){
        btnumber = 9;
        setFixedSize(750,730);
        this->setGeometry(
                    QStyle::alignedRect(
                        Qt::LeftToRight,
                        Qt::AlignCenter,
                        this->size(),
                        qApp->desktop()->availableGeometry()
                    )
                );
        ui->gombok->setFixedSize(670,470);
        ui->tarto->setFixedSize(670, 101);
        _model.setSize(9);
    }else if(ui->rbt19->isChecked())
    {
        btnumber = 19;
        setFixedSize(1080,970);
        this->setGeometry(
                    QStyle::alignedRect(
                        Qt::LeftToRight,
                        Qt::AlignCenter,
                        this->size(),
                        qApp->desktop()->availableGeometry()
                    )
                );
        ui->gombok->setFixedSize(1000,710);
        ui->tarto->setFixedSize(1000, 101);
        _model.setSize(19);
    }
    startGame(btnumber);
    ui->label_2->setText(tr("Fekete"));
}

void GoWidget::startGame(int value)
{
    clearLayout(ui->gombtarto);
    btnholder.clear();
    btnholder.resize(value);
    for(int i=0; i<value; ++i)
    {
        btnholder[i].resize(value);
        for(int j=0; j<value; ++j)
        {
            QPushButton* gomb = new QPushButton("",this);
            gomb->setStyleSheet("background-color: rgb(172, 115, 57);");
            connect(gomb, SIGNAL(clicked()), this, SLOT(pshbtn_slot()));
            ui->gombtarto->addWidget(gomb, i, j);
            btnholder[i][j] = gomb;
        }
    }
    _model.newGame();
}

void GoWidget::pshbtn_slot()
{
    QPushButton* senderButton = dynamic_cast<QPushButton*> (QObject::sender());
    int pozicio = ui->gombtarto->indexOf(senderButton);
    _model.round(pozicio / btnumber , pozicio % btnumber);
    if(_model.currentPlayer() == 1){
        ui->label_2->setText(tr("Fekete"));
    }else{
        ui->label_2->setText(tr("Fehér"));
    }
}

GoWidget::~GoWidget()
{
    delete ui;
}


void GoWidget::clearLayout(QLayout *layout)
{
    if (layout) {
        while(layout->count() > 0){
            QLayoutItem *item = layout->takeAt(0);
            QWidget* widget = item->widget();
            if(widget)
                delete widget;
            delete item;
        }
    }
}

void GoWidget::loadGame()
{
    if(_model.loadIn(_loadGameWidget->selectedGame()))
    {
        clearLayout(ui->gombtarto);
        btnumber = _model.getSize();
        switch (_model.getSize()) {
            case 5: {setFixedSize(610,680);
                ui->gombok->setFixedSize(530,420);
                ui->tarto->setFixedSize(530, 101);
                this->setGeometry( QStyle::alignedRect( Qt::LeftToRight,  Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));
                break;
            }
            case 9: {
                setFixedSize(750,730);
                ui->gombok->setFixedSize(670,470);
                ui->tarto->setFixedSize(670, 101);
                this->setGeometry( QStyle::alignedRect( Qt::LeftToRight,  Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));
                break;
            }
            case 19: {
                setFixedSize(1080,970);
                ui->gombok->setFixedSize(1000,710);
                ui->tarto->setFixedSize(1000, 101);
                this->setGeometry( QStyle::alignedRect( Qt::LeftToRight,  Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));
                break;
             }
        }
        btnholder.clear();
        btnholder.resize(_model.getSize());
        for(int i=0; i< _model.getSize(); ++i)
        {
            btnholder[i].resize(_model.getSize());
            for(int j= 0; j< _model.getSize(); ++j)
            {
                QPushButton* gomb = new QPushButton("",this);
                gomb->setStyleSheet("background-color: rgb(172, 115, 57);");
                if(_model._data(i,j) == 1)
                {
                    gomb->setIcon(QIcon(":/images/stone1.png"));
                }else if(_model._data(i,j) == 2) gomb->setIcon(QIcon(":/images/stone2.png"));
                connect(gomb, SIGNAL(clicked()), this, SLOT(pshbtn_slot()));
                ui->gombtarto->addWidget(gomb, i, j);
                btnholder[i][j] = gomb;
            }
        }
        if(_model.currentPlayer() == 1){
            ui->label_2->setText(tr("Fekete"));
        }else{
            ui->label_2->setText(tr("Fehér"));
        }
        QMessageBox::information(this, tr("Go! "), trUtf8("Játék betöltve, következik: ") + ((_model.currentPlayer() == GoModel::PlayerB) ? "Fekete" : "Fehér") + "!");
    }else
    {
        QMessageBox::warning(this, tr("Go! "), trUtf8("A játék betöltése sikertelen!"));
    }
}

void GoWidget::saveGame()
{
    if(_model.saveOut(_saveGameWidget->selectedGame()))
    {
        QMessageBox::information(this, tr("Go! "), trUtf8("Játék sikeresen mentve!"));
    }else
    {
        QMessageBox::warning(this, tr("Go! "), trUtf8("A játék mentése sikertelen!"));
    }
}

void GoWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_L && QApplication::keyboardModifiers() == Qt::ControlModifier)
    {
            if(btnumber >0){
               _loadGameWidget = new GoLoadWidget();
                connect(_loadGameWidget, SIGNAL(accepted()), this, SLOT(loadGame()));
                _loadGameWidget->setGameList(_model.saveGameList());
                _loadGameWidget->open();
            }
    }

    if (event->key() == Qt::Key_S && QApplication::keyboardModifiers() == Qt::ControlModifier)
        {
            if(btnumber >0){
                _saveGameWidget = new GoSaveWidget();
                connect(_saveGameWidget, SIGNAL(accepted()), this, SLOT(saveGame()));
                _saveGameWidget->setGameList(_model.saveGameList());
                _saveGameWidget->open();
             }
       }
}
