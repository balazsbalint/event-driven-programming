#ifndef GOWIDGET_H
#define GOWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QStyle>
#include <QDesktopWidget>
#include <QLayout>
#include <QMessageBox>
#include <QProcess>
#include "gomodel.h"
#include "goloadwidget.h"
#include "gosavewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class GoWidget; }
QT_END_NAMESPACE

class GoWidget : public QWidget
{
    Q_OBJECT

protected:
    void keyPressEvent(QKeyEvent *event);
public:
    GoWidget(QWidget *parent = 0);
    ~GoWidget();
    void startGame(int value);
    void clearLayout(QLayout *layout);
private slots:
    void nwgmbt_clicked();
    void pshbtn_slot();
    void _model_round_done(int a, int b, GoModel::Player Player);
    void _model_colour_back(int a, int b);
    void _model_gameWon(GoModel::Player Player);
    void _model_gameDraw();
    void loadGame();
    void saveGame();

private:
    Ui::GoWidget *ui;
    GoModel _model;
    int btnumber;
    QVector<QVector<QPushButton*> > btnholder;
    GoSaveWidget* _saveGameWidget;
    GoLoadWidget* _loadGameWidget;
};
#endif // GOWIDGET_H
