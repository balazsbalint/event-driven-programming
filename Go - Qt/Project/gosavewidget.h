#ifndef GOSAVEWIDGET_H
#define GOSAVEWIDGET_H
#include <QDialog>
#include <QPushButton>
#include <QListWidget>

class GoSaveWidget : public QDialog
{
    Q_OBJECT
public:
    explicit GoSaveWidget(QWidget *parent = 0);
    void setGameList(QVector<QString> mentesiLista);
    int selectedGame() const { return _listWidget->currentRow(); }
protected:
    QPushButton* _okButton;
    QPushButton* _cancelButton;
    QListWidget* _listWidget;
};

#endif // GOSAVEWIDGET_H
