#ifndef GOADATKEZELO_H
#define GOADATKEZELO_H
#include <QString>
#include <QVector>
#include <QDateTime>
#include <QFileInfo>
#include <QFile>
#include <QTextStream>

class GoAdatKezelo
{
public:
    explicit GoAdatKezelo(){}
    bool loadGame(int gameIndex, QVector<int> &saveGameData);
    bool saveGame(int gameIndex, const QVector<int> &saveGameData);

    QVector<QString> saveGameList() const;

};

#endif // GOADATKEZELO_H
