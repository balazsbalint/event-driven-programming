#include "goloadwidget.h"
#include <QMessageBox>

GoLoadWidget::GoLoadWidget(QWidget *parent) :
    GoSaveWidget(parent)
{
    setWindowTitle("A Go Játék betöltése");
    disconnect(_okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(_okButton, SIGNAL(clicked()), this, SLOT(okButton_Clicked()));
}

void GoLoadWidget::okButton_Clicked()
{
    if (_listWidget->currentItem()->text() == "üres")
    {
        QMessageBox::warning(this, trUtf8("Go!"), trUtf8("Nincs játék kiválasztva!"));
        return;
    }
    accept();
}

